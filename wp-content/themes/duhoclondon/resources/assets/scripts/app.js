import 'jquery';
import 'bootstrap';
import slick from 'slick-carousel/slick/slick.min.js';
import venobox from 'venobox/venobox/venobox.min.js';
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';
import 'jquery-validation/dist/jquery.validate.js';

$(document).ready(function() {

    $('.venobox').venobox();

    //menu primary
    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });
	
    //home pupil testimonial
    if ($('.pupil-testimonial-content .row .msc-listing article').length > 3) {
        $('.pupil-testimonial-content .row .msc-listing').slick({
            // infinite: false, ko ap dung dc voi col bootstrap
            slidesToShow: 3,
            slidesToScroll: 1,
            pauseOnHover: false,
            // autoplay: true,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    //search truong
    $('#find-school').on('input', function() {
        var value_oninput = $(this).val();

        $('.tab-pane ul li a').each(function(){
            var value_search = $(this).html();

            if(value_oninput){
                if (value_search.includes(value_oninput)) {
                    $(this).parent().css('display', 'block');
                }else{
                    $(this).parent().css('display', 'none');
                }
            }else{
                $(this).parent().css('display', 'block');
            }
        });
    });

    //form register
    // $(".registerform-duhoc").click(function(){
        $("#form-register-email").validate({
            rules: {
                firstname: {
                    required : true,
                    remote: {
                        url: ajax_obj.ajax_url,
                        type: "post",
                        data: {
                            'firstname': function() {
                                return $( "#firstname" ).val();
                            },
                            'lastname': function() {
                                return $( "#lastname" ).val();
                            },
                            'action': 'check_user_name'
                        }
                    }
                },
                lastname: {
                    required : true,
                },
                tel : {
                    required: true,
                    number: true,
                },
                email : {
                    required: true,
                    email: true,
                    remote: {
                        url: ajax_obj.ajax_url,
                        type: "post",
                        data: {
                            email: function() {
                                return $( "#email" ).val();
                            },
                            'action': 'check_user_email'
                        }
                    }
                },
                pass : {
                    required: true,
                    minlength :6
                },
                repass : {
                    required: true,
                    minlength: 6,
                    equalTo: "#pass"
                },
                checkbox : {
                    required: true,
                }
            },
            messages : {
                firstname: {
                    required: input_empty,
                    remote : error_firstname,
                },
                lastname: {
                    required: input_empty,
                },
                tel : {
                    required : input_empty,
                    number : error_tel,
                },
                email: {
                    required: input_empty,
                    email : error_email,
                },
                pass: {
                    required: input_empty, 
                    minlength: error_pass,
                },
                repass: {
                    required: input_empty,
                    minlength: error_pass,
                    equalTo: error_repass,
                },
                checkbox: {
                    required: error_checkbox,
                }
            },
            // submitHandler: function(form) {
            //     $.ajax({
            //         type: "POST",
            //         url: ajax_obj.ajax_url,
            //         data: {
            //             'action': 'Register_du_hoc',
            //             'firstname' : $( "#firstname" ).val(),
            //             'lastname' : $( "#lastname" ).val(),
            //             'email' : $( "#email" ).val(),
            //         },


            //         success: function(response) {
            //             console.log(response);
            //         }
            //     });
            // }
        });
    // });

    //custom requiredmsg input, select
    var elements = $("input, select");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                 e.target.setCustomValidity(e.target.getAttribute("requiredmsg"));
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }

    //find course : select country -> list school
    $("#course-country").change(function(e) {
        var value_course_country = $(this).val();
        // console.log(value_course_country);

        //custom requiredmsg input, select
        if(value_course_country == "") {
            console.log('rong');
            $('#course-university').removeAttr("required","");
        } else {
            console.log('ko rong');
            $('#course-university').attr("required","");
        }

        jQuery.ajax({
            type: "POST",
            url: ajax_obj.ajax_url,
            data: {
                action: 'List_university',
                value_course_country : value_course_country,
            },
            success:function(response){
                var data = jQuery.parseJSON(response);
                $('#course-university').html(data.result);
            }
        });

    });
});