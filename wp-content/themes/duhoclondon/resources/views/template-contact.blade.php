@extends('layouts.full-width')

@section('banner')

    @if (!empty($banner_img_check))
        <img src="{{ $banner_img_check }}">
    @endif

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-contact">
        <div class="container">

            <div class="page-contact-content">
                <div class="row">
    
                    <section class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 content-page">
                        <div class="page-title">
                            <h1>
                                {{ $name_page }}
                            </h1>
                        </div>

                        @empty (!get_the_content())
                        <div class="page-content">
                            {!! wpautop(the_content()) !!}
                        </div>
                        @endempty
                    </section>

                    <section class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 contact-map">
                        {!! $contact_map !!}
                    </section>
                    
                </div>
            </div>

        </div>
    </section>

    @endwhile
@endsection