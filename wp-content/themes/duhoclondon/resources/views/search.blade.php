@extends('layouts.full-width')

@section('banner')

    @if (!empty($banner_img_check))
        <img src="{{ $banner_img_check }}">
    @endif

@endsection

@section('content')

    @if (!have_posts())
        <div class="alert alert-warning">
            {{ _e('Sorry, no results found.', 'duhoc') }}
        </div>
    @endif

    <section class="page-category">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content">
                    
                    <section class="content-page">
                        <div class="page-title">
                            <h1>
                                {{ _e('Search for', 'duhoc') }} : <?php echo '['.$_GET['s'].']'; ?>
                            </h1>
                        </div>
                    </section>

                    <section class="page-post">
                        <div class="page-post-content">
                            @while(have_posts())

                                {!! the_post() !!}

                                    @php
                                        $data = [
                                            'id' => get_the_ID(),
                                            'title' => get_the_title(),
                                            'url' => get_permalink(),
                                            'excerpt' => get_the_excerpt()
                                        ];
                                    @endphp
                                    {!!  view('partials.sections.content-page-post', $data)  !!}

                            @endwhile
                        </div>
                    </section>

                    <div class="page-navi">
                        <div class="container">
                            @php
                                global $wp_query;

                                $big = 999999999;

                                echo paginate_links(array(
                                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                                    'format' => '?paged=%#%',
                                    'current' => max(1, get_query_var('paged')),
                                    'total' => $wp_query->max_num_pages,
                                    'prev_text' => __('<i class="fa fa-angle-left"></i>'),
                                    'next_text' => __('<i class="fa fa-angle-right"></i>'),
                                ));
                            @endphp
                        </div>
                    </div>

                    @php
                        view('partials/page-next-steps');
                    @endphp

                </div>

                <?php get_sidebar();?>

            </div>
        </div>
    </section>

@endsection

