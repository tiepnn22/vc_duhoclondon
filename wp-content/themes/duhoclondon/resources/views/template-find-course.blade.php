@extends('layouts.full-width')


@section('banner')

    @empty (!$banner_img_check)
        <img src="{{ $banner_img_check }}">
    @endempty

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-find-course">
        <div class="container">

            <div class="page-title">
                @empty (!$name_page)
                    <h1>{{ $name_page }}</h1>
                @endempty
            </div>

            <div class="page-find-course-content">
                <div class="row">
                    
                    @foreach ( $find_course_cat as $find_course_cat_kq )
                        @php
                            $term_childs = get_term_children( $find_course_cat_kq, 'courses-category' );
                            $count = count($term_childs);

                            if($count > 0) {
                                foreach ( $term_childs as $term_childs_kq ) {
                                                    
                                    $image_cat = get_field('image_cat', 'courses-category_'.$term_childs_kq );
                                    $get_term_find_course = get_term($term_childs_kq);

                                    $data = [
                                        'image' => $image_cat,
                                        'title' => $get_term_find_course->name,
                                        'excerpt' => $get_term_find_course->description,
                                        'url' => get_term_link( $get_term_find_course )
                                    ];

                                    @endphp
                                        {!!  view('partials.sections.content-find-course', $data)  !!}
                                    @php
                                }
                            }
                        @endphp
                    @endforeach

                </div>
            </div>

        </div>
    </section>

    @endwhile
@endsection