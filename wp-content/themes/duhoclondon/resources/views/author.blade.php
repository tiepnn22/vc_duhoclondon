@extends('layouts.full-width')



@section('content')


<?php
	//dat nuoc
	global $wpdb;
	$countries_db = $wpdb->get_results( "SELECT * FROM countries");

	//ngon ngu
    $student_language = array(
        'French' => 'French',
        'Italian' => 'Italian',
        'Brazilian Portuguese' => 'Brazilian Portuguese',
        'Russian' => 'Russian',
        'Latin-American Spanish' => 'Latin-American Spanish',
        'Spanish' => 'Spanish',
        'Hindi' => 'Hindi',
        'Korean' => 'Korean',
        'Traditional Chinese' => 'Traditional Chinese',
        'Indonesian' => 'Indonesian',
        'Japanese' => 'Japanese',
        'English' => 'English',
        'Chinese' => 'Chinese',
        'German' => 'German',
        'Vietnamese' => 'Vietnamese',
        'Thai' => 'Thai',
    );

    // echo $user_name = get_the_author();
    // echo get_the_title();

    // $user_id = get_the_ID();
    $user_id =  get_current_user_id();

    if(isset($_POST['save-info-account'])) {
        update_user_meta( $user_id, 'student_id', $_POST['student-id'] );
        update_user_meta( $user_id, 'student_job', $_POST['student-job'] );
        update_user_meta( $user_id, 'student_name', $_POST['student-name'] );
        update_user_meta( $user_id, 'student_gender', $_POST['student-gender'] );
        update_user_meta( $user_id, 'student_birthday', $_POST['student-birthday'] );
        update_user_meta( $user_id, 'student_language', $_POST['student-language'] );
        update_user_meta( $user_id, 'student_email_one', $_POST['student-email-one'] );
        update_user_meta( $user_id, 'student_email_two', $_POST['student-email-two'] );

		update_user_meta( $user_id, 'service_interested', $_POST['service-interested'] );

        update_user_meta( $user_id, 'contact_address_type', $_POST['contact-address-type'] );
        update_user_meta( $user_id, 'contact_tel', $_POST['contact-tel'] );
        update_user_meta( $user_id, 'contact_address', $_POST['contact-address'] );
        update_user_meta( $user_id, 'contact_zipcode', $_POST['contact-zipcode'] );
        
        update_user_meta( $user_id, 'info_type', $_POST['info-type'] );
        update_user_meta( $user_id, 'info_passport_number', $_POST['info-passport-number'] );
        update_user_meta( $user_id, 'info_granted_country', $_POST['info-granted-country'] );
        update_user_meta( $user_id, 'info_value_to', $_POST['info-value-to'] );

        update_user_meta( $user_id, 'degree_type', $_POST['degree-type'] );
        update_user_meta( $user_id, 'degree_name', $_POST['degree-name'] );
        update_user_meta( $user_id, 'degree_school', $_POST['degree-school'] );
        update_user_meta( $user_id, 'degree_specialized', $_POST['degree-specialized'] );
        update_user_meta( $user_id, 'degree_status', $_POST['degree-status'] );
        update_user_meta( $user_id, 'degree_graduation', $_POST['degree-graduation'] );
        update_user_meta( $user_id, 'degree_language', $_POST['degree-language'] );
        update_user_meta( $user_id, 'degree_country', $_POST['degree-country'] );

        update_user_meta( $user_id, 'english_certification_exam', $_POST['english-certification-exam'] );
        update_user_meta( $user_id, 'english_take_the_exam', $_POST['english-take-the-exam'] );
        update_user_meta( $user_id, 'english_ielts_score', $_POST['english-ielts-score'] );

        update_user_meta( $user_id, 'his_work_employees', $_POST['his-work-employees'] );
        update_user_meta( $user_id, 'his_work_lever', $_POST['his-work-lever'] );
        update_user_meta( $user_id, 'his_work_month', $_POST['his-work-total-month'] );
        update_user_meta( $user_id, 'his_work_at', $_POST['his-work-at'] );

        update_user_meta( $user_id, 'socical_participation', $_POST['socical-participation'] );
        update_user_meta( $user_id, 'participation_facebook', $_POST['participation-facebook'] );
        update_user_meta( $user_id, 'participation_google', $_POST['participation-google'] );
        update_user_meta( $user_id, 'participation_twitter', $_POST['participation-twitter'] );
        update_user_meta( $user_id, 'participation_other', $_POST['participation-other'] );

    }
?>

<section class="page-account">
    <div class="container">
    	<div class="page-account-content">

        	<section class="content-page">
				<div class="page-title">
					<h1>{{ _e('Your account', 'duhoc') }}</h1>
				</div>
				<div class="page-desc">
						{{ _e('Please update the details we can find the course that best suits you', 'duhoc') }}
				</div>
			</section>

            <form class="form-submit-account" method="post">
            	<div class="item account-info-user">
            		<div class="title-section">
						<h2>{{ _e('Personal information', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Student code', 'duhoc') }}</label>
							    <input class="form-control" name="student-id" type="text" value="{{ get_user_meta( $user_id, 'student_id', true ) }}" >
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Title', 'duhoc') }}</label>
							    <input class="form-control" name="student-job" type="text" value="{{ get_user_meta( $user_id, 'student_job', true ) }}" >
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Name', 'duhoc') }}</label>
							    <input class="form-control" name="student-name" type="text" value="{{ get_user_meta( $user_id, 'student_name', true ) }}" >
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Gender', 'duhoc') }}</label>
							    <input class="form-control" name="student-gender" type="text" value="{{ get_user_meta( $user_id, 'student_gender', true ) }}" >
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Birthday', 'duhoc') }}</label>
							    <input class="form-control" name="student-birthday" type="date" value="{{ get_user_meta( $user_id, 'student_birthday', true ) }}" >
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Mother language', 'duhoc') }}</label>
								<select name="student-language">
									<option value="">{{ _e('Please choose', 'duhoc') }}</option>

<?php
	foreach ($student_language as $student_language_kq) {
?>
		<option value="<?php echo $student_language_kq; ?>" 
		<?php echo (get_user_meta( $user_id, 'student_language', true ) == $student_language_kq) ? 'selected' : ''; ?>>
			<?php echo $student_language_kq; ?>
		</option>
<?php
	}
?>
								</select>
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Address Email one', 'duhoc') }}</label>
							    <input class="form-control" name="student-email-one" type="email" value="{{ get_user_meta( $user_id, 'student_email_one', true ) }}" >
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Address Email two', 'duhoc') }}</label>
							    <input class="form-control" name="student-email-two" type="email" value="{{ get_user_meta( $user_id, 'student_email_two', true ) }}" >
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-my-service">
            		<div class="title-section">
						<h2>{{ _e('Our services', 'duhoc') }}</h2>
					</div>
					<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('What service do you care?', 'duhoc') }}</label>
								<div class="checkbox-group">
									<div class="checkbox-item">
									    <input type="checkbox" id="mot" name="service-interested[]" value="I'd like to study overseas"
									    <?php
											$service_interested = get_user_meta( $user_id, 'service_interested', true );
											foreach ($service_interested as $service_interested_kq) {
												echo ($service_interested_kq == "I'd like to study overseas") ? 'checked' : '';
											}
									    ?>>
									    <label for="mot"><span>{{ _e("I'd like to study overseas", 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									    <input type="checkbox" id="hai" name="service-interested[]" value="I'd like to take an IELTS test"
									   	<?php
											$service_interested = get_user_meta( $user_id, 'service_interested', true );
											foreach ($service_interested as $service_interested_kq) {
												echo ($service_interested_kq == "I'd like to take an IELTS test") ? 'checked' : '';
											}
									    ?>>
									    <label for="hai"><span>{{ _e("I'd like to take an IELTS test", 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									    <input type="checkbox" id="ba" name="service-interested[]" value="I'm interested in services to make my arrival easier"
									   	<?php
											$service_interested = get_user_meta( $user_id, 'service_interested', true );
											foreach ($service_interested as $service_interested_kq) {
												echo ($service_interested_kq == "I'm interested in services to make my arrival easier") ? 'checked' : '';
											}
									    ?>>
									    <label for="ba"><span>{{ _e("I'm interested in services to make my arrival easier", 'duhoc') }}</span></label>
									</div>
								</div>
							</div>
						</div>
					</div>
            	</div>

            	<div class="item account-info-contact">
            		<div class="title-section">
						<h2>{{ _e('Contact Info', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Address type', 'duhoc') }}</label>
								<div class="checkbox-group">
									<div class="checkbox-item">
									   <input type="radio" id="bon" name="contact-address-type" value="Home" 
									   <?php echo (get_user_meta( $user_id, 'contact_address_type', true ) == 'Home') ? 'checked' : ''; ?>>
									   <label for="bon"><span>{{ _e('Home', 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="radio" id="nam" name="contact-address-type" value="Destination"
									   <?php echo (get_user_meta( $user_id, 'contact_address_type', true ) == 'Destination') ? 'checked' : ''; ?>>
									   <label for="nam"><span>{{ _e('Destination', 'duhoc') }}</span></label>
									</div>
								</div>
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Phone', 'duhoc') }}</label>
							    <input class="form-control" name="contact-tel" type="tel" value="{{ get_user_meta( $user_id, 'contact_tel', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Address', 'duhoc') }}</label>
							    <input class="form-control" name="contact-address" type="text" value="{{ get_user_meta( $user_id, 'contact_address', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('ZIP code', 'duhoc') }}</label>
							    <input class="form-control" name="contact-zipcode" type="text" value="{{ get_user_meta( $user_id, 'contact_zipcode', true ) }}">
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-info-detail">
            		<div class="title-section">
						<h2>{{ _e('Infomation Details', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Type', 'duhoc') }}</label>
								<div class="checkbox-group">
									<div class="checkbox-item">
									   <input type="radio" id="sau" name="info-type" value="Passport"
									   <?php echo (get_user_meta( $user_id, 'info_type', true ) == 'Passport') ? 'checked' : ''; ?>>
									   <label for="sau"><span>{{ _e('Passport', 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="radio" id="bay" name="info-type" value="Drivers licence"
									   <?php echo (get_user_meta( $user_id, 'info_type', true ) == 'Drivers licence') ? 'checked' : ''; ?>>
									   <label for="bay"><span>{{ _e('Drivers Licence', 'duhoc') }}</span></label>
									</div>
								</div>
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Number', 'duhoc') }}</label>
							    <input class="form-control" name="info-passport-number" type="number" value="{{ get_user_meta( $user_id, 'info_passport_number', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Issued in the country', 'duhoc') }}</label>
								<select name="info-granted-country">
									<option value="">{{ _e('Pick a starting point', 'duhoc') }}</option>
<?php
	foreach ($countries_db as $countries_db_kq) {
?>
		<option value="<?php echo $countries_db_kq->name; ?>" 
		<?php echo (get_user_meta( $user_id, 'info_granted_country', true ) == $countries_db_kq->name) ? 'selected' : ''; ?>>
			<?php echo $countries_db_kq->name; ?>
		</option>
<?php
	}
?>
								</select>
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Valid until', 'duhoc') }}</label>
							    <input class="form-control" name="info-value-to" type="date" value="{{ get_user_meta( $user_id, 'info_value_to', true ) }}">
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-history-study">
            		<div class="title-section">
						<h2>{{ _e('Study history', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Type of degree', 'duhoc') }}</label>
								<div class="checkbox-group">
									<div class="checkbox-item">
									   <input type="radio" id="tam" name="degree-type" value="Undergraduate"
									   <?php echo (get_user_meta( $user_id, 'degree_type', true ) == 'Undergraduate') ? 'checked' : ''; ?>>
									   <label for="tam"><span>{{ _e('Undergraduate', 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="radio" id="chin" name="degree-type" value="Post-Graduate"
									   <?php echo (get_user_meta( $user_id, 'degree_type', true ) == 'Post-Graduate') ? 'checked' : ''; ?>>
									   <label for="chin"><span>{{ _e('Post-Graduate', 'duhoc') }}</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="radio" id="muoi" name="degree-type" value="Doctorate"
									   <?php echo (get_user_meta( $user_id, 'degree_type', true ) == 'Doctorate') ? 'checked' : ''; ?>>
									   <label for="muoi"><span>{{ _e('Doctorate', 'duhoc') }}</span></label>
									</div>
								</div>
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Degree Name', 'duhoc') }}</label>
							    <input class="form-control" name="degree-name" type="text" value="{{ get_user_meta( $user_id, 'degree_name', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('University', 'duhoc') }}</label>
							    <input class="form-control" name="degree-school" type="text" value="{{ get_user_meta( $user_id, 'degree_school', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Field', 'duhoc') }}</label>
							    <input class="form-control" name="degree-specialized" type="text" value="{{ get_user_meta( $user_id, 'degree_specialized', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Status', 'duhoc') }}</label>
								<select name="degree-status">
									<option value="">{{ _e('Please choose', 'duhoc') }}</option>
									<option value="Graduated" 
									<?php echo (get_user_meta( $user_id, 'degree_status', true ) == 'Graduated') ? 'selected' : ''; ?>>Graduated</option>
									<option value="Deferred" 
									<?php echo (get_user_meta( $user_id, 'degree_status', true ) == 'Deferred') ? 'selected' : ''; ?>>Deferred</option>
									<option value="In Process" 
									<?php echo (get_user_meta( $user_id, 'degree_status', true ) == 'In Process') ? 'selected' : ''; ?>>In Process</option>
								</select>
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Graduation Date', 'duhoc') }}</label>
							    <input class="form-control" name="degree-graduation" type="date" value="{{ get_user_meta( $user_id, 'degree_graduation', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Learn in any language', 'duhoc') }}</label>
								<select name="degree-language">
									<option value="">{{ _e('Please choose', 'duhoc') }}</option>

<?php
	foreach ($student_language as $student_language_kq) {
?>
		<option value="<?php echo $student_language_kq; ?>" 
		<?php echo (get_user_meta( $user_id, 'degree_language', true ) == $student_language_kq) ? 'selected' : ''; ?>>
			<?php echo $student_language_kq; ?>
		</option>
<?php
	}
?>

								</select>
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Study in any country', 'duhoc') }}</label>
								<select name="degree-country">
									<option value="">{{ _e('Select country', 'duhoc') }}</option>
<?php
	foreach ($countries_db as $countries_db_kq) {
?>
		<option value="<?php echo $countries_db_kq->name; ?>" 
		<?php echo (get_user_meta( $user_id, 'degree_country', true ) == $countries_db_kq->name) ? 'selected' : ''; ?>>
			<?php echo $countries_db_kq->name; ?>
		</option>
<?php
	}
?>
								</select>
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-level-english">
            		<div class="title-section">
						<h2>{{ _e('English level', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Have you ever been or intend to take any English certificate?', 'duhoc') }}</label>
								<select name="english-certification-exam">
									<option value="">{{ _e('Select certificate', 'duhoc') }}</option>
									<option value="Yes"
									<?php echo (get_user_meta( $user_id, 'english_certification_exam', true ) == 'Yes') ? 'selected' : ''; ?>>Yes taken</option>
									<option value="No"
									<?php echo (get_user_meta( $user_id, 'english_certification_exam', true ) == 'No') ? 'selected' : ''; ?>>No</option>
									<option value="Intend to Take"
									<?php echo (get_user_meta( $user_id, 'english_certification_exam', true ) == 'Intend to Take') ? 'selected' : ''; ?>>Intend to Take</option>
								</select>
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Have you ever been or intend to take any English exam?', 'duhoc') }}</label>
								<select name="english-take-the-exam">
									<option value="">{{ _e('Select Exam', 'duhoc') }}</option>
									<option value="IELTS"
									<?php echo (get_user_meta( $user_id, 'english_take_the_exam', true ) == 'IELTS') ? 'selected' : ''; ?>>IELTS</option>
									<option value="TOEFL"
									<?php echo (get_user_meta( $user_id, 'english_take_the_exam', true ) == 'TOEFL') ? 'selected' : ''; ?>>TOEFL</option>
									<option value="TOEIC"
									<?php echo (get_user_meta( $user_id, 'english_take_the_exam', true ) == 'TOEIC') ? 'selected' : ''; ?>>TOEIC</option>
								</select>
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('IELTS score', 'duhoc') }}</label>
							    <input class="form-control" name="english-ielts-score" type="text" value="{{ get_user_meta( $user_id, 'english_ielts_score', true ) }}">
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-history-work">
            		<div class="title-section">
						<h2>{{ _e('Employment history', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Employer', 'duhoc') }}</label>
								<input class="form-control" name="his-work-employees" type="text" value="{{ get_user_meta( $user_id, 'his_work_employees', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Position', 'duhoc') }}</label>
							    <input class="form-control" name="his-work-lever" type="text" value="{{ get_user_meta( $user_id, 'his_work_lever', true ) }}">
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('Total number of months', 'duhoc') }}</label>
							    <input class="form-control" name="his-work-total-month" type="number" value="{{ get_user_meta( $user_id, 'his_work_month', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Employment in the country', 'duhoc') }}</label>
								<select name="his-work-at">
									<option value="">{{ _e('Select country', 'duhoc') }}</option>

<?php
	foreach ($countries_db as $countries_db_kq) {
?>
		<option value="<?php echo $countries_db_kq->name; ?>" 
		<?php echo (get_user_meta( $user_id, 'his_work_at', true ) == $countries_db_kq->name) ? 'selected' : ''; ?>>
			<?php echo $countries_db_kq->name; ?>
		</option>
<?php
	}
?>
								</select>
							</div>
            			</div>
            		</div>
            	</div>

            	<div class="item account-socical">
            		<div class="title-section">
						<h2>{{ _e('Social Network', 'duhoc') }}</h2>
					</div>
            		<div class="row">
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">{{ _e('What social network are you in?', 'duhoc') }}</label>
								<div class="checkbox-group">
									<div class="checkbox-item">
									   <input type="checkbox" id="mot1" name="socical-participation[]" value="Facebook"
									    <?php
											$socical_participation = get_user_meta( $user_id, 'socical_participation', true );
											foreach ($socical_participation as $socical_participation_kq) {
												echo ($socical_participation_kq == "Facebook") ? 'checked' : '';
											}
									    ?>>
									   <label for="mot1"><span>Facebook</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="checkbox" id="hai1" name="socical-participation[]" value="Google"
									    <?php
											$socical_participation = get_user_meta( $user_id, 'socical_participation', true );
											foreach ($socical_participation as $socical_participation_kq) {
												echo ($socical_participation_kq == "Google") ? 'checked' : '';
											}
									    ?>>
									   <label for="hai1"><span>Google+</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="checkbox" id="ba1" name="socical-participation[]" value="Twitter"
									    <?php
											$socical_participation = get_user_meta( $user_id, 'socical_participation', true );
											foreach ($socical_participation as $socical_participation_kq) {
												echo ($socical_participation_kq == "Twitter") ? 'checked' : '';
											}
									    ?>>
									   <label for="ba1"><span>Twitter</span></label>
									</div>
									<div class="checkbox-item">
									   <input type="checkbox" id="bon1" name="socical-participation[]" value="Khác"
									    <?php
											$socical_participation = get_user_meta( $user_id, 'socical_participation', true );
											foreach ($socical_participation as $socical_participation_kq) {
												echo ($socical_participation_kq == "Khác") ? 'checked' : '';
											}
									    ?>>
									   <label for="bon1"><span>{{ _e('Other', 'duhoc') }}</span></label>
									</div>
								</div>
							</div>
            			</div>
            			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="wrap-group">
								<label for="">Facebook URL</label>
							    <input class="form-control" name="participation-facebook" type="text" value="{{ get_user_meta( $user_id, 'participation_facebook', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">Google+ URL</label>
							    <input class="form-control" name="participation-google" type="text" value="{{ get_user_meta( $user_id, 'participation_google', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">Twitter URL</label>
							    <input class="form-control" name="participation-twitter" type="text" value="{{ get_user_meta( $user_id, 'participation_twitter', true ) }}">
							</div>
							<div class="wrap-group">
								<label for="">{{ _e('Other', 'duhoc') }}</label>
							    <input class="form-control" name="participation-other" type="text" value="{{ get_user_meta( $user_id, 'participation_other', true ) }}">
							</div>
            			</div>
            		</div>
            	</div>


				<div class="wrap-group-submit">
					<input class="btn btn-primary btn-submit" type="reset" value="{{ _e('Reset', 'duhoc') }}">
	                <input class="btn btn-primary btn-submit" type="submit" value="{{ _e('Save', 'duhoc') }}" name="save-info-account">
	            </div>
            </form>

        </div>
    </div>
</section>


@endsection
