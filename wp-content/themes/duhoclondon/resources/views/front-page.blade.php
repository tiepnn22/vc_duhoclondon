@extends('layouts.full-width')

@section('content')


	<section class="home-banner">
		<div class="home-banner-content">

			<?php
				echo do_shortcode("[vc-slider name='Slider Homepage']");
			?>
			
		</div>
	</section>


	@empty (!$home_news_feature)
		<section class="home-news-feature">
	        <div class="container">
				<div class="news-feature-content">
					<div class="row">

						@foreach ( $home_news_feature as $home_news_feature_kq )
						    @php
						        $data = [
						            'image' => $home_news_feature_kq['image'],
						            'title' => $home_news_feature_kq['title'],
						            'excerpt' => $home_news_feature_kq['excerpt'],
						            'url' => $home_news_feature_kq['url']
						        ];
						    @endphp
						    {!!  view('partials.sections.content-news-feature', $data)  !!}
					    @endforeach

		    		</div>
		    	</div>
		    </div>
		</section>
	@endempty


	@empty (!$home_service_support)
		<section class="home-service-support">
	        <div class="container">

				<div class="title-section">
					<h2>{{ get_cat_name( $home_service_support ) }}</h2>
				</div>

				<div class="service-support-content">

		            <div class="row">
	                    @php
	                        $shortcode = "[listing cat=$home_service_support layout='partials.sections.content-service-support' per_page='4']";
	                        echo do_shortcode($shortcode);
	                    @endphp
					</div>

					<a class="read-more-section" href="{{ get_term_link( get_term( $home_service_support ) ) }}">
						{{ $home_service_support_readmore }}
					</a>

				</div>
			</div>
		</section>
	@endempty


	@empty (!$home_pupil_share)
		<section class="home-pupil-share">
	        <div class="container">

				<div class="title-section">
					<h2>{{ $home_pupil_share_title }}</h2>
				</div>

				<div class="pupil-share-content">
		            
		            <div class="row">

						@foreach ( $home_pupil_share as $home_pupil_share_kq )
						    @php
						        $data = [
						            'title' => $home_pupil_share_kq['title'],
						            'excerpt' => $home_pupil_share_kq['excerpt'],
						            'url' => $home_pupil_share_kq['url']
						        ];
						    @endphp
						    {!!  view('partials.sections.content-pupil-share', $data)  !!}
					    @endforeach

		    		</div>

					<a class="read-more-section" href="{{ $home_pupil_share_url }}">
						{{ $home_pupil_share_readmore }}
					</a>

		    	</div>
		    </div>
		</section>
	@endempty


	<section class="home-pupil-testimonial" style="background-image: url({{ $home_pupil_testimonial_bg }})">
        <div class="container">

			<div class="title-section">
				<h2>{{ $home_pupil_testimonial_title }}</h2>
			</div>

			<div class="pupil-testimonial-content">

	            <div class="row">

				    @php
				        $shortcode = '[listing post_type="pupils" layout="partials.sections.content-pupil-testimonial" per_page="-1"]';
				        echo do_shortcode($shortcode);
				    @endphp

				</div>

			</div>
		</div>
	</section>


	<section class="home-my-house">
        <div class="container">

			<div class="title-section">
				<h2>{{ $home_my_house_title }}</h2>
			</div>

			<div class="my-house-content">
	            
	            <div class="row">

					@php
						$terms_school = get_terms('university-category', array(
						    'parent'=> 0,
						    'hide_empty' => false
						) );

						
						foreach( $terms_school as $terms_school_kq ) {
							$home_my_house_kq = $terms_school_kq->term_id;

							$image_cat = get_field('image_cat', 'university-category_'.$home_my_house_kq );
							$get_term_house = get_term($home_my_house_kq);

					        $data = [
					        	'image' => $image_cat,
					            'title' => $get_term_house->name,
					            'excerpt' => $get_term_house->description,
					            'url' => get_term_link( $get_term_house )
					        ];
				    @endphp
						    {!!  view('partials.sections.content-my-house', $data)  !!}
			    	@php
						}
					@endphp

				</div>

			</div>
		</div>
	</section>


	<section class="home-news">
        <div class="container">
			<div class="home-news-content">
				<div class="row">

					@empty (!$home_news_new)
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 news-new">
							<div class="title-section">
								<h2>{{ get_cat_name( $home_news_new ) }}</h2>
							</div>

							<div class="news-new-content">
			                    @php
			                        $shortcode = "[listing cat=$home_news_new layout='partials.sections.content-home-news' per_page='3']";
			                        echo do_shortcode($shortcode);
			                    @endphp
							</div>
						</div>
					@endempty

					@empty (!$home_even_highlight)
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 even-highlight">
							<div class="title-section">
								<h2>{{ get_cat_name( $home_even_highlight ) }}</h2>
							</div>

							<div class="even-highlight-content">
			                    @php
			                        $shortcode = "[listing cat=$home_even_highlight layout='partials.sections.content-home-news' per_page='3']";
			                        echo do_shortcode($shortcode);
			                    @endphp
							</div>
						</div>
					@endempty

				</div>
			</div>
		</div>
	</section>


	@empty (!$home_course_popular)
		<section class="home-course-popular">
	        <div class="container">

				<div class="title-section">
					<h2>{{ $home_course_popular_title }}</h2>
				</div>

				<div class="course-popular-content">

		            <div class="row">

						@foreach ( $home_course_popular as $home_course_popular_kq )
							@php
								$image_cat = get_field('image_cat_home', 'courses-category_'.$home_course_popular_kq );
								$get_term_popular = get_term($home_course_popular_kq);
							@endphp

						    @php
						        $data = [
						        	'image' => $image_cat,
						            'title' => $get_term_popular->name,
						            'excerpt' => $get_term_popular->description,
						            'url' => get_term_link( $get_term_popular )
						        ];
						    @endphp
						    {!!  view('partials.sections.content-course-popular', $data)  !!}
					    @endforeach

					</div>

					<a class="read-more-section" href="{{ $home_course_popular_url }}">
						{{ $home_course_popular_readmore }}
					</a>

				</div>
			</div>
		</section>
	@endempty


	<section class="home-best-university">
        <div class="container">

			<div class="title-section">
				<h2>{{ $home_best_university_title }}</h2>
			</div>

			<div class="best-university-content">
	            
	            <div class="row">

					@php
						$shortcode = "[listing post_type='university' layout='partials.sections.content-best-university' per_page=8]";
						echo do_shortcode($shortcode);
					@endphp

				</div>

				<a class="read-more-section" href="{{ $home_best_university_url }}">
					{{ $home_best_university_readmore }}
				</a>

			</div>
		</div>
	</section>

	
	@empty (!$home_our_member)
		<section class="home-our-member">
	        <div class="container">

				<div class="title-section">
					<h2>{{ $home_our_member_title }}</h2>
				</div>

				<div class="our-member-content">
		            
		            <div class="row">

						@foreach ( $home_our_member as $home_our_member_kq )

						    @php
						        $data = [
						        	'id' => $home_our_member_kq
						        ];
						    @endphp
						    {!!  view('partials.sections.content-our-member', $data)  !!}
					    @endforeach

					</div>

				</div>
			</div>
		</section>
	@endempty


@endsection

