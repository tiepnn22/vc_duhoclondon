@extends('layouts.full-width')




@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-taxonomy-university">
        <div class="container">

            <div class="page-title">
                <h1>{{ $name_page }}</h1>
            </div>

            <div class="page-taxonomy-university-content">
                <div class="row">

                    @php
                        $shortcode = "[listing post_type='university' taxonomy='university-category(".$id_page.")' layout='partials.sections.content-taxonomy-university' per_page=-1]";
                        echo do_shortcode($shortcode);
                    @endphp

                </div>
            </div>

        </div>
    </section>

    @endwhile
@endsection