@extends('layouts.full-width')




@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-find-course">
        <div class="container">

            <div class="page-title">
                <h1>{{ get_data_language( 'Tìm khoá học', 'Find course' ) }}</h1>
            </div>

            <div class="page-find-course-content">
                <div class="row">

                    @php
                        foreach ($terms_courses as $terms_courses_kq) {
                            $terms_courses_id = $terms_courses_kq->term_taxonomy_id;

                            $term_childs = get_term_children( $terms_courses_id, 'courses-category' );
                            $count = count($term_childs);

                            if($count > 0) {
                                foreach ( $term_childs as $term_childs_kq ) {
                                                    
                                    $image_cat = get_field('image_cat', 'courses-category_'.$term_childs_kq );
                                    $get_term_find_course = get_term($term_childs_kq);

                                    $data = [
                                        'image' => $image_cat,
                                        'title' => $get_term_find_course->name,
                                        'excerpt' => $get_term_find_course->description,
                                        'url' => get_term_link( $get_term_find_course )
                                    ];

                                    echo view('partials.sections.content-find-course', $data);
                                    
                                }
                            }
                        }
                    @endphp

                </div>
            </div>

        </div>
    </section>

    @endwhile
@endsection