@extends('layouts.full-width')

@section('content')

	<section class="single-school">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-school-content">

					<section class="content-page">
						<div class="page-title">
							<h1>{{ $name_page }}</h1>
						</div>
					</section>

                    <section class="single-course-info">
                        <div class="course-info">
                            {!! wpautop($cat_school_info_avatar) !!}
                        </div>
                        <div class="course-detail-info">
                            <div class="row">
                                @foreach ( $cat_school_info as $cat_school_info_kq )
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 item">
                                        <span><img src="{{ $cat_school_info_kq['image'] }}" ></span>
                                        <span>{{ $cat_school_info_kq['title'] }}</span>
                                        <span>{{ $cat_school_info_kq['excerpt'] }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>

					<section class="introduction-school">
                        {!! wpautop($single_course_introduction) !!}
					</section>

					<section class="certificate-school">
						<div class="title-article">
							<h3>{{ $cat_school_certificate_title }}</h3>
						</div>
						<div class="certificate-school-content">
							<div class="row">
								<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 item">
									{!! wpautop($cat_school_certificate_col_1) !!}
								</div>
								<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 item">
									{!! wpautop($cat_school_certificate_col_2) !!}
								</div>
								<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 item">
									{!! wpautop($cat_school_certificate_col_3) !!}
								</div>
							</div>
						</div>
					</section>

                    <section class="page-post">
                        <div class="title-section">
                            <h2>
                                {{ $page_post_single_course_title }}
                            </h2>
                        </div>
    
                        <div class="page-post-content">
                            @php
                                $shortcode = "[listing cat=$page_post_single_course layout='partials.sections.content-page-post-single-course' per_page=2 ]";
                                echo do_shortcode($shortcode);
                            @endphp
                        </div>
    
                        <div class="page-desc">
                            {!! wpautop($page_post_single_course_desc) !!}
                        </div>
    
                    </section>
    
                    @php
                        view('partials/page-next-steps');
                    @endphp

                </div>
                
                @php
                    view('sidebar-course');
                @endphp
				
			</div>
		</div>
    </section>
    
@endsection