<header class="header">
	<div class="header-top">
		<div class="container">

		    <div class="language">
	        	{!! do_action('wpml_add_language_selector') !!}
		    </div>
    
			<div class="header-top-content">

				<div class="logo">
					<a href="{{ get_option('home') }}">
						<img src="{{ get_option('header_logo') }}" alt="{{ get_option('blogname') }}">
					</a>
					<div class="name-company">
						{{ get_data_language( get_option('header_slogan'), get_option('header_slogan_en') ) }}
					</div>
				</div>

				@php
					echo view('partials/search-box');
				@endphp

	            <div class="header-right">
	            	<a class="header-address" href="{{ get_data_language( get_option('header_url_address'), get_option('header_url_address_en') ) }}">
	            		<i class="fa fa-map-marker"></i>
	            		<span>
							{{ _e('Address', 'duhoc') }}
						</span>
	            	</a>
	            	<a class="header-calendar" href="{{ get_data_language( get_option('header_url_calendar'), get_option('header_url_calendar_en') ) }}">
	            		<i class="fa fa-calendar"></i>
	            		<span>
							{{ _e('Make a calendar', 'duhoc') }}
						</span>
	            	</a>
                    
                    @if(is_user_logged_in())
                        @php
	                        $user_id = get_current_user_id();
	                        $current_user = wp_get_current_user(); //array info user on admin
	                        $profile_url = get_author_posts_url($user_id); //link page author
	                        $edit_profile_url  = get_edit_profile_url($user_id); //link profile trong admin
                    	@endphp

	                    <div class="header-user-account">
		                    <a href="{{ $profile_url }}">{{ $current_user->display_name }}</a>
		                    <a href="{{ wp_logout_url(home_url()) }}" > ({{ _e('Logout', 'duhoc') }})</a>
	                    </div>
                    @else
		            	<div class="header-account">
		            		<div class="header-account-login">
		            			<div class="header-account-login-content">

									<a href="#loginModal" role="button" data-toggle="modal" class="btn btn-default btn-lg">
										<span>{{ _e('Login / Register', 'duhoc') }}</span>
									</a>

								</div>
							</div>
		            	</div>
						@php
							echo view('partials/login-socical');
						@endphp
                    @endif

	            </div>
			</div>
		</div>
	</div>

	<nav class="menu-primary">
		<div class="container">
		    <div class="main-menu">
		        @if (has_nav_menu('main-menu'))
		            {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
		        @endif
		    </div>
		    <div class="mobile-menu"></div>

			@php
				echo view('partials/search-box');
			@endphp
		</div>
	</nav>
</header>


