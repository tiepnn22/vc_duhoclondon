<section class="single-blog">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-blog-content">
                
                <section class="content-page">
                    <div class="page-title">
                        <h1>{{ the_title() }}</h1>
                    </div>

                    <div class="page-content">
                        {!! wpautop(the_content()) !!}
                    </div>
                </section>

                @empty (!$page_post)
                    @php
                        $shortcode = "[page_post id=$page_post title='$page_post_title' numberpost url='partials.sections.content-page-post']";
                        echo do_shortcode($shortcode);
                    @endphp                
                @endempty

                @php
                    view('partials/page-next-steps');
                @endphp

            </div>

            <?php get_sidebar();?>

        </div>
    </div>
</section>