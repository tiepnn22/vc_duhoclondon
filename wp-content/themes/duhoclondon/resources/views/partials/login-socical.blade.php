<!-- Modal -->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <span class="modal-title">{{ get_data_language( 'Hãy bắt đầu', "Let's start" ) }}</span>
                <span class="modal-title-small">{{ get_data_language( 'Đăng kí hoặc đăng nhập', 'Register or login' ) }}</span>
            </div>
            <div class="modal-body">
            	<div class="modal-login">
            		<div class="modal-login-socical">
						@php
							echo do_shortcode('[nf_social_url providers="facebook"]');
						@endphp
						
						@php
							echo do_shortcode('[nf_social_url providers="google"]');
						@endphp

	            	</div>
	            	<div class="modal-login-or">
	            		<span>{{ get_data_language( 'hoặc', 'or' ) }}</span>
	            	</div>
	            	<a class="modal-login-email" href="{{ get_option('header_url_register') }}">
	            		<i class="fa fa-envelope"></i> {{ get_data_language( 'Đăng kí / Đăng nhập bằng Email', 'Register / Login by Email' ) }}
	            	</a>
            	</div>
            </div>
        </div>
    </div>
</div>