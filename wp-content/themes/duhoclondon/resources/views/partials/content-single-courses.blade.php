<section class="single-course">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-course-content">

				<section class="content-page">
					<div class="page-title">
						@empty (!$name_page)
							<h1>{{ $name_page }}</h1>
						@endempty
					</div>
				</section>
				
				<section class="single-course-info">
					<div class="course-info">
						{!! wpautop($single_course_info_avatar) !!}
					</div>

					<div class="course-detail-info">
						<div class="row">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-course-1.png') }})" >
								</span>
								<span>{{ _e('Degree', 'duhoc') }}</span>
								<span>{{ $single_course_info_col_1 }}</span>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-course-2.png') }})" >
								</span>
								<span>{{ _e('Next semester', 'duhoc') }}</span>
								<span>{{ $single_course_info_col_2 }}</span>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-course-3.png') }})" >
								</span>
								<span>{{ _e('Total Course Cost', 'duhoc') }}</span>
								<span>{{ format_price( $single_course_info_col_3 ).' đ' }}</span>
							</div>
						</div>
					</div>
				</section>

				<section class="page-post submit-file-course">
					<div class="title-article">
						<h3>{{ $single_course_certificate_title }}</h3>
					</div>
					<div class="submit-file-course-content">
						<div class="row">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 item">
								{!! wpautop($single_course_certificate_col_1) !!}
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 item">
								{!! wpautop($single_course_certificate_col_2) !!}
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 item">
								{!! wpautop($single_course_certificate_col_3) !!}
							</div>
						</div>
						{!! wpautop($single_course_certificate_excerpt) !!}
					</div>
				</section>

				<section class="why-your-course">
					{!! wpautop($single_course_introduction) !!}
				</section>

				<section class="page-post star-tuition-course">
					<div class="star-tuition-course-content">
						{!! wpautop($single_course_tuition_meta) !!}

						<div class="table-responsive">
							 <table class="table table-bordered">
			                	<thead>
			                		<tr>
			                			<th>{{ _e('Start the opening', 'duhoc') }}</th>
			                			<th>{{ _e('Course duration', 'duhoc') }}</th>
			                			<th>{{ _e('Tuition', 'duhoc') }}</th>
			                			<th>{{ _e('Place', 'duhoc') }}</th>
			                		</tr>
			                	</thead>
			                    <tbody>
									@foreach ( $single_course_tuition_info as $single_course_tuition_info_kq )
									<tr>
										<td>{{ $single_course_tuition_info_kq['star'] }}</td>
										<td>{{ $single_course_tuition_info_kq['time'] }}</td>
										<td>{{ format_price( $single_course_tuition_info_kq['cost'] ).' đ' }}</td>
										<td>
											{{ $single_course_tuition_info_kq['address'] }}
											<a data-gall="iframe" title="{{ $single_course_tuition_info_kq['address'] }}" class="venobox" data-vbtype="iframe" href="{{ $single_course_tuition_info_kq['map'] }}">
                        						<i class="fa fa-map-marker"></i> {{ _e('Map', 'duhoc') }}
                        					</a>
										</td>
									</tr>
								    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</section>

				<section class="ratings-reviews-course">
					<div class="title-article">
						<h3>{{ $single_course_ratings_title }}</h3>
					</div>
					<div class="ratings-reviews-course-content">
						<div class="row">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 item">
								{!! wpautop($single_course_ratings_col_1) !!}
							</div>
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 item">
								{!! wpautop($single_course_ratings_col_2) !!}
							</div>
						</div>
					</div>
				</section>

				@empty (!$page_post_single_course)
				<section class="page-post">
					<div class="title-section">
						<h2>
							{{ $page_post_single_course_title }}
						</h2>
					</div>
					
					<div class="page-post-content">
						@php
							$shortcode = "[listing cat=$page_post_single_course layout='partials.sections.content-page-post-single-course' per_page=2 ]";
					        echo do_shortcode($shortcode);
				        @endphp
					</div>

					<div class="page-desc">
						{!! wpautop($page_post_single_course_desc) !!}
					</div>
				</section>
				@endempty

                @php
                    view('partials/page-next-steps');
                @endphp

			</div>

            @php
                view('sidebar-course');
            @endphp

		</div>
	</div>
</section>