<section class="single-school">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-school-content">

				<section class="content-page">
					<div class="page-title">
						@empty (!$name_page)
							<h1>{{ $name_page }}</h1>
						@endempty
					</div>
				</section>

                <section class="single-course-info">
                    <div class="course-info">
                        {!! wpautop($cat_school_info_avatar) !!}
                    </div>

					<div class="course-detail-info">
						<div class="row">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-school-1.png') }})" >
								</span>
								<span>{{ _e('Field type university', 'duhoc') }}</span>
								<span>{{ $cat_school_info_col_1 }}</span>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-school-2.png') }})" >
								</span>
								<span>{{ _e('International student', 'duhoc') }}</span>
								<span>{{ $cat_school_info_col_2 }}</span>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 item">
								<span>
									<img src="{{ asset_image('3x1.png') }}" style="background-image: url({{ asset_image('icon/single-school-3.png') }})" >
								</span>
								<span>{{ _e('Opportunity to work overtime', 'duhoc') }}</span>
								<span>{{ $cat_school_info_col_3 }}</span>
							</div>
						</div>
					</div>
                </section>

				<section class="introduction-school">
                    {!! wpautop($cat_school_introduction) !!}
				</section>

				@empty (!$cat_school_certificate_title)
				<section class="certificate-school">
					<div class="title-article">
						<h3>{{ $cat_school_certificate_title }}</h3>
					</div>
					<div class="certificate-school-content">
						<div class="row">
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 item">
								{!! wpautop($cat_school_certificate_col_1) !!}
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 item">
								{!! wpautop($cat_school_certificate_col_2) !!}
								<a href="{{ get_data_language( get_option('header_url_calendar'), get_option('header_url_calendar_en') ) }}">
									{{ _e('Register now', 'duhoc') }}
								</a>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 item">
								{!! wpautop($cat_school_certificate_col_3) !!}
							</div>
						</div>
					</div>
				</section>
				@endempty

				@empty (!$page_post_single_course)
                <section class="page-post">
                    <div class="title-section">
                        <h2>
                            {{ $page_post_single_course_title }}
                        </h2>
                    </div>

                    <div class="page-post-content">
                        @php
                            $shortcode = "[listing cat=$page_post_single_course layout='partials.sections.content-page-post-single-course' per_page=2 ]";
                            echo do_shortcode($shortcode);
                        @endphp
                    </div>

                    <div class="page-desc">
                        {!! wpautop($page_post_single_course_desc) !!}
                    </div>
                </section>
                @endempty

                @php
                    view('partials/page-next-steps');
                @endphp

            </div>
            
            @php
                view('sidebar-course');
            @endphp
			
		</div>
	</div>
</section>