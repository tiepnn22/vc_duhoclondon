<div class="search-box">
    <form action="{!! esc_url( home_url( '/' ) ) !!}">
        <input type="text" placeholder="{{ _e('Enter search keywords', 'duhoc') }}" id="search-box" name="s" value="{!! get_search_query() !!}">
<!--         <div class="search-icon">
            <i class="fa fa-search" aria-hidden="true"></i>
        </div> -->
        <button type="submit" class="search-icon">
        	<i class="fa fa-search" aria-hidden="true"></i>
        </button>
    </form>
</div>