<section class="single-event">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-event-content">
            	<section class="content-page">
					<div class="page-title">
						<h1>{{ the_title() }}</h1>
					</div>

					<div class="page-content">
						{!! wpautop(the_content()) !!}
					</div>
				</section>
                
            </div>

            @php
                view('sidebar-single-event');
            @endphp

        </div>
    </div>
</section>