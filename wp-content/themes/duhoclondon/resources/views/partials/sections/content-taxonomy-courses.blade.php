@php

	$single_course_info = get_field('single_course_info', $id);

@endphp

<article class="item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'course') }})" alt="{{ $title }}">
		</a>
		<div class="title">
			<a href="{{ $url }}">
				<h3>
					{{ $title }}
				</h3>
			</a>
		</div>
	</figure>
	<div class="info">
		<div class="info-content">
			<span>
				{{ _e('Degree', 'duhoc') }}
			</span>
			<span>
				{{ get_field('single_course_info_col_1', $id) }}
			</span>
			<span>
				{{ _e('Next start date', 'duhoc') }}
			</span>
			<span>
				{{ get_field('single_course_info_col_2', $id) }}
			</span>
			<span>
				{{ _e('Total Course Cost', 'duhoc') }}
			</span>
			<span>
				@php
					$single_course_info_col_3 = get_field('single_course_info_col_3', $id);
				@endphp
				@empty (!$single_course_info_col_3)
					{{ format_price( $single_course_info_col_3 ).' đ' }}
				@endempty
			</span>
		</div>
	</div>
</article>
