<article class="item">
    <figure>
        <a href="{{ $url }}">
            <img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'page-post') }})" alt="{{ $title }}">
        </a>
    </figure>
    <div class="info">
        <div class="info-content">
            <div class="title">
                <a href="{{ $url }}">
                    <h3>
                        {{ $title }}
                    </h3>
                </a>
            </div>
            <div class="desc">
                @php
                    if (get_the_excerpt() != '') {
                        $excerpt = createExcerptFromContent(get_the_excerpt(), 30);
                    } else {
                        $excerpt = '';
                    }
                @endphp
                {{ $excerpt }}
            </div>
        </div>
    </div>
</article>