<article class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'even') }})" alt="{{ $title }}">
			</a>
		</figure>
    	<div class="info">
    		<div class="info-content">
        		<div class="title">
                    <a href="{{ $url }}">
                        <h3>
                            {{ $title }}
                        </h3>
                    </a>
            	</div>
            	<div class="desc">
                    {!! get_field('even_excerpt') !!}
            	</div>
            	<a class="read-more" href="{{ $url }}">
            		Đăng ký ngay
            	</a>
            </div>
        </div>
    </div>
</article>


