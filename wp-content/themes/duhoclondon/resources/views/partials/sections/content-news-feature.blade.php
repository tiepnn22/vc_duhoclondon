<article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
                <img src="{{ asset_image('4x1.png') }}" style="background-image: url({{ $image }})" alt="{{ $title }}">
			</a>
		</figure>
    	<div class="info">
    		<div class="info-content">
        		<div class="title">
        			<a href="{{ $url }}">
                		<h3>
                    		{{ $title }}
                		</h3>
                	</a>
            	</div>
            	<div class="desc">
                    @php
                        if ($excerpt != '') {
                            $excerpt = createExcerptFromContent($excerpt, 20);
                        } else {
                            $excerpt = '';
                        }
                    @endphp
                    {{ $excerpt }}
            	</div>
            </div>
        </div>
    </div>
</article>
