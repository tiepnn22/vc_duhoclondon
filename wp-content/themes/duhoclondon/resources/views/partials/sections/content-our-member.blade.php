<article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
	<div class="item">
		<figure>
			<img src="{{ asset_image('2x1.png') }}" style="background-image: url({{ getPostImage($id, 'home-member') }})" alt="{{ $title }}">
		</figure>
    </div>
</article>