<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'home-service') }})" alt="{{ $title }}">
			</a>
		</figure>
    	<div class="info">
    		<div class="info-content">
            	<div class="desc">
					@php
						if (get_the_excerpt() != '') {
							$excerpt = createExcerptFromContent(get_the_excerpt(), 30);
						} else {
							$excerpt = '';
						}
					@endphp
					{{ $excerpt }}
            	</div>
            	<a class="read-more" href="{{ $url }}">Xem thêm</a>
            </div>
        </div>
    </div>
</article>