<article class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
    <div class="item">
        <figure>
            <a href="{{ $url }}">
                <img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'page-post-recent') }})" alt="{{ $title }}">
            </a>
        </figure>
        <div class="info">
            <div class="info-content">
                <div class="title">
                    <a href="{{ $url }}">
                        <h3>
                            {{ $title }}
                        </h3>
                    </a>
                </div>
                <div class="desc">
                    @php
                        if (get_the_excerpt() != '') {
                            $excerpt = createExcerptFromContent(get_the_excerpt(), 18);
                        } else {
                            $excerpt = '';
                        }
                    @endphp
                    {{ $excerpt }}
                </div>
            </div>
        </div>
    </div>
</article>
