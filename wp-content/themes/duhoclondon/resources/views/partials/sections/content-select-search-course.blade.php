@php
    $taxonomy_name = 'courses-category';

    $term_slug = get_data_language( $term_slug, $term_slug_en );

    $term = get_term_by('slug', $term_slug, $taxonomy_name);
    $term_id = $term->term_id;
    $term_name = $term->name;

    $term_childs = get_term_children( $term_id, $taxonomy_name );
    $count = count($term_childs);

    if($count >= 0) {
    @endphp
        <select name="{{ $select_name }}" id="{{ $select_name }}">
            <option value="">{{ $term_name }}</option>
            @php
                foreach ( $term_childs as $child ) {
                    $term = get_term_by( 'id', $child, $taxonomy_name );

                    if($term->parent == $term_id) {

                    @endphp
                        <option 
                            
                            @empty (!$mon_hoc)
                                @php
                                    if($mon_hoc == $term->term_id) { echo 'selected="selected"'; }
                                @endphp
                            @endempty
                            @empty (!$bac_hoc)
                                @php
                                    if($bac_hoc == $term->term_id) { echo 'selected="selected"'; }
                                @endphp
                            @endempty

                             value="{{ $term->term_id }}">
                             {{ $term->name }}
                             
                         </option>
                    @php

                    }
                }
            @endphp
        </select>
    @php
    }
@endphp