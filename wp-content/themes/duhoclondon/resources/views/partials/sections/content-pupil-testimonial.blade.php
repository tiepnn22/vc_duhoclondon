<article class="col-4">
	<div class="item">
		<figure>
			<img src="{{ asset_image('4x3.png') }}" style="background-image: url({{ getPostImage($id, 'home-pupil-read') }})" alt="{{ $title }}">
		</figure>
    	<div class="info">
    		<div class="info-content">
        		<div class="title">
            		<h3>
                		{{ $title }}
            		</h3>
            	</div>
            	<div class="desc">
            		{{ $excerpt }}
            	</div>
            </div>
        </div>
    </div>
</article>