<article class="item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'single-course') }})" alt="{{ $title }}">
		</a>
	</figure>
	<div class="info">
		<div class="info-content">
        	<div class="desc">
				@php
					if ($content != '') {
						$excerpt = createExcerptFromContent($content, 30);
					} else {
						$excerpt = '';
					}
				@endphp
				{!! wpautop($excerpt) !!}
        	</div>
        </div>
    </div>
</article>
