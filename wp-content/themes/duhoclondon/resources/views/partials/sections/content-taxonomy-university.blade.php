<article class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
        <div class="item">
            <figure>
                <a href="{{ $url }}">
                    <img src="{{ asset_image('3x2.png') }}" style="background-image: url({{ getPostImage($id, 'home-find-course') }})" alt="{{ $title }}">
                </a>
            </figure>
            <div class="title">
                <a href="{{ $url }}">
                    <h3>
                        {{ $title }}
                    </h3>
                </a>
            </div>
        </div>
    </article>