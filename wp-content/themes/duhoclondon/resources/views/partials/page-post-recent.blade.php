<section class="page-post-recent">
    <div class="title-section">
        <h2>{{ get_cat_name( $page_post_recent ) }}</h2>
    </div>
    <div class="page-post-recent-content">
        <div class="row">
            @php
                $shortcode = "[listing cat=$page_post_recent layout='partials.sections.content-page-post-recent' per_page='6']";
                echo do_shortcode($shortcode);
            @endphp
        </div>
    </div>
</section>