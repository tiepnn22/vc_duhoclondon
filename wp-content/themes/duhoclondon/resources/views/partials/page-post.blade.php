<section class="page-post">
    @empty (!$page_post_title)
        <div class="title-section">
            <h2>
                {{ $page_post_title }}
            </h2>
        </div>
    @endempty
    <div class="page-post-content">
        @php
            $shortcode = "[listing cat=$page_post layout='partials.sections.content-page-post' per_page='6']";
            echo do_shortcode($shortcode);
        @endphp
    </div>
</section>