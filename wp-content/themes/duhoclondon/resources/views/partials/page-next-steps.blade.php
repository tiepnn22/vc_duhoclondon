<section class="page-next-steps">
    <div class="title-section">
        <h2>{{ _e('Next steps', 'duhoc') }}</h2>
    </div>
    <div class="next-steps">
        <div class="row">
            <?php
                if(ICL_LANGUAGE_CODE == 'vi'){
                    dynamic_sidebar('sidebar-next-step');
                } elseif (ICL_LANGUAGE_CODE == 'en') {
                    dynamic_sidebar('sidebar-next-step-en');
                }
            ?>
        </div>
    </div>
</section>