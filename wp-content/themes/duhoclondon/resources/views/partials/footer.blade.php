<footer class="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<address class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column">
					<?php dynamic_sidebar('footer-1'); ?>
				</address>
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column">
					<?php dynamic_sidebar('footer-2'); ?>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column">
					<?php dynamic_sidebar('footer-3'); ?>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column">
					<?php dynamic_sidebar('footer-4'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<!-- <div class="row"> -->
				<div class="footer-copyright">
					<?php dynamic_sidebar('footer-5'); ?>
				</div>
				<div class="footer-policy">
					<?php dynamic_sidebar('footer-6'); ?>
				</div>
			<!-- </div> -->
		</div>
	</div>
</footer>

<div class="popup-mobile">
    <div class="popup-mobile-content">
        <a class="popup-mobile-calendar" href="{{ get_data_language( get_option('header_url_calendar'), get_option('header_url_calendar_en') ) }}">
            {{ _e('making appointment', 'duhoc') }}
        </a>
        <a class="popup-mobile-call" href="tel:{{ get_option('header_tell') }}">
            {{ _e('Call now', 'duhoc') }}
        </a>
    </div>
</div>