<div class="social-sidebar">
    <ul>
        <li>
            <a href="{{ get_option('socical_facebook') }}">
                <i class="fa fa-facebook"></i>
            </a>
        </li>
        <li>
            <a href="{{ get_option('socical_google') }}">
                <i class="fa fa-google-plus"></i>
            </a>
        </li>
        <li>
            <a href="{{ get_option('socical_twitter') }}">
                <i class="fa fa-twitter"></i>
            </a>
        </li>
        <li>
            <a href="{{ get_option('socical_linkedIn') }}">
                <i class="fa fa-linkedin"></i>
            </a>
        </li>
    </ul>
</div>