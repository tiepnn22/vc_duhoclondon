<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ title() }}</title>
    {!! wp_head() !!}

    <!--sdk facebook-->
    <script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=349811632194610";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<!--share this-->
	<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5ad6b6f6022fe00013ed9816&product=custom-share-buttons"></script>

	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5aebd3fc5f7cdf4f0533d9e9/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
	<!--End of Tawk.to Script-->

	<!--variable form register user-->
	<?php
		if(ICL_LANGUAGE_CODE == 'vi'){
	?>
			<script>
				var input_empty = "Vui lòng nhập trường này";
				var error_firstname = "Tài khoản này đã tồn tại";
				var error_tel = "Vui lòng nhập số";
				var error_email = "Vui lòng nhập đúng email";
				var error_pass = "Mật khẩu tối thiểu 6 kí tự để bảo mật";
				var error_repass = "Mật khẩu không giống với mật khẩu ở trên";
				var error_checkbox = "Vui lòng chấp nhận điều khoản và chính sách";

				var error_success_register = "Đăng ký thành công !";
			</script>
	<?php
		} elseif (ICL_LANGUAGE_CODE == 'en') {
	?>
			<script>
				var input_empty = "Please enter this field";
				var error_firstname = "This account is existed";
				var error_tel = "Please enter the number";
				var error_email = "Please enter valid email";
				var error_pass = "Password has least 6 characters to security";
				var error_repass = "Password's not match";
				var error_checkbox = "Please accept Term of use and Privacy Policy";

				var error_success_register = "Register Success !";
			</script>
	<?php
		}
	?>
	
</head>
