@extends('layouts.full-width')



@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-register">
        <div class="container">
        	<div class="page-register-content">
	            <div class="row">
	                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 login-email">
	                	
	                	<section class="content-page">
							<div class="page-title">
								<h1>{{ _e('Easily log in instantly with your social account', 'duhoc') }}</h1>
							</div>
							<div class="page-desc">
								{{ _e('We support login with Facebook, Google account.', 'duhoc') }}
							</div>
							<br>

							<a class="read-more" href="#loginModal" role="button" data-toggle="modal">
								{{ _e('Sign in with your social account', 'duhoc') }}
							</a>

							<div class="page-title">
								<h1>{{ _e('Login by Email', 'duhoc') }}</h1>
							</div>
							<div class="page-desc">
								{{ _e('Do you already have an account? Log in to your personal page.', 'duhoc') }}
							</div>
						</section>
				
				        @php
				            wp_login_form();
				        @endphp

	                </div>

					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 register-email">

	                	<section class="content-page">
							<div class="page-title">
								<h1>{{ _e('Do not have an account? Sign up now by Email', 'duhoc') }}</h1>
							</div>
							<div class="page-desc">
								{{ _e('Register for a free trial, save and explore the course, talk to a counselor, and apply to the school. All information fields are required.', 'duhoc') }}
							</div>
						</section>

						@php
							global $wp;
							$page_url = home_url( $wp->request );
						@endphp

			            <form action="{{ site_url('wp-admin/admin-post.php') }}" class="form-register-email" id="form-register-email" method="post">
			            	<input type="hidden" name="action" value="sign_up">
			            	<input type="hidden" name="page_url" value="{{ $page_url }}">
			            	<input type="hidden" name="error_success_register" value="{{ get_data_language( 'Đăng ký thành công !', 'Register Success !' ) }}">
			                <div class="wrap-group">
			                	<label for="firstname">{{ _e('First name', 'duhoc') }}</label>
			                    <input class="form-control" name="firstname" type="text" id="firstname">
			                </div>
			                <div class="wrap-group">
			                	<label for="lastname">{{ _e('Last name', 'duhoc') }}</label>
			                    <input class="form-control" name="lastname" type="text" id="lastname">
			                </div>
			                <div class="wrap-group">
			                	<label for="tel">{{ _e('Phone', 'duhoc') }}</label>
			                    <input class="form-control" name="tel" type="text" id="tel">
			                </div>
			                <div class="wrap-group">
			                	<label for="email">{{ _e('Your email address', 'duhoc') }}</label>
			                    <input class="form-control" name="email" type="text" id="email">
			                </div>
			                <div class="wrap-group">
			                	<label for="pass">{{ _e('Your password', 'duhoc') }}</label>
			                    <input class="form-control" name="pass" type="password" id="pass">
			                </div>
			                <div class="wrap-group">
			                	<label for="repass">{{ _e('Confirm password', 'duhoc') }}</label>
			                    <input class="form-control" name="repass" type="password" id="repass">
			                </div>
							<div class="wrap-group">
								<input type="checkbox" name="checkbox" id="checkbox">
								<label for="checkbox">
									{{ _e('I have read and accept the terms and privacy policy of IDP.', 'duhoc') }}
								</label>
							</div>

							<div class="wrap-group-submit">
				                <input class="btn btn-primary btn-submit registerform-duhoc" name="registerform-duhoc" type="submit" value="{{ _e('Create new account', 'duhoc') }}">
				            </div>
			            </form>

					</div>

	            </div>
	        </div>
        </div>
    </section>

    @endwhile
@endsection