@extends('layouts.full-width')


@section('banner')

    @empty (!$banner_img_check)
        <img src="{{ $banner_img_check }}">
    @endempty

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-event">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-event-content">
                        
                        <section class="content-page">
                            <div class="page-title">
                                @empty (!$name_page)
                                    <h1>{{ $name_page }}</h1>
                                @endempty
                            </div>
                        </section>

                        <section class="page-post">
                            @empty (!$page_post_title)
                                <div class="title-section">
                                    <h2>
                                        {{ $page_post_title }}
                                    </h2>
                                </div>
                            @endempty
                            <div class="page-post-content">
                                <div class="row">
                                    @php
                                        $shortcode = '[listing post_type="event" layout="partials.sections.content-page-event" per_page="6"]';
                                        echo do_shortcode($shortcode);
                                    @endphp
                                </div>
                            </div>
                        </section>
                        
                    </div>

                    <?php get_sidebar();?>

                </div>
            </div>
        </section>

    @endwhile
@endsection