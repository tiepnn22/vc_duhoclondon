<aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 sidebar sidebar-common">
	<div class="sidebar-common-content">

		<?php
			if(ICL_LANGUAGE_CODE == 'vi'){
				dynamic_sidebar('sidebar-common');
			} elseif (ICL_LANGUAGE_CODE == 'en') {
				dynamic_sidebar('sidebar-common-en');
			}
		?>

	</div>

	<div class="sharethis-inline-share-buttons"></div>

</aside>