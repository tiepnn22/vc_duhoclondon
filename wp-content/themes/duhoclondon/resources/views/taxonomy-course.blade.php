@extends('layouts.full-width')
@section('content')
    <section class="page-course">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-course-content">

                    <section class="content-page">
                        <div class="page-title">
                            <h1>{{ $name_page }}</h1>
                        </div>
                    </section>

                    <div class="search-course">
                        <form action="{{ get_data_language( '/search-khoa-hoc/', '/en/search-course/' ) }}" method="post" class="form-search-course">

                            <div class="order-course">
                                {{ _e('Sorted by:', 'duhoc') }}
                                <select name="course-order" id="course-order">
                                    <option value="DESC">{{ _e('Latest:', 'duhoc') }}</option>
                                    <option value="ASC">{{ _e('Oldest:', 'duhoc') }}</option>
                                </select>
                            </div>

                            <div class="wrap-group-course">

                                <div class="wrap-group">
                                    @php
                                        $data = [
                                            'term_slug' => 'mon-hoc',
                                            'term_slug_en' => 'subject',
                                            'select_name' => 'course-subjects'
                                        ];
                                    @endphp
                                    {!!  view('partials.sections.content-select-search-course', $data)  !!}
                                </div>

                                <div class="wrap-group">
                                    @php
                                        $data = [
                                            'term_slug' => 'bac-hoc',
                                            'term_slug_en' => 'level-learning',
                                            'select_name' => 'course-level'
                                        ];
                                    @endphp
                                    {!!  view('partials.sections.content-select-search-course', $data)  !!}
                                </div>

                                <div class="wrap-group">
                                    @php
                                        $terms_school = get_terms('university-category', array(
                                            'parent'=> 0,
                                            'hide_empty' => false
                                        ) );
                                    @endphp
                                        
                                    <select name="course-country" id="course-country">
                                        <option value="">{{ _e('Country', 'duhoc') }}</option>
                                        @foreach( $terms_school as $terms_school_kq )
                                            <option value="{{ $terms_school_kq->term_id }}">{{ $terms_school_kq->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="wrap-group">
                                    <select name="course-university" id="course-university" requiredmsg="{{ get_data_language( 'Vui lòng chọn trường', 'Please select a school' ) }}">
                                        <option value="">{{ _e('University', 'duhoc') }}</option>
                                        @php
                                            $shortcode = "[listing post_type='university' layout='partials.sections.content-select-search-course-school' per_page=-1]";
                                            echo do_shortcode($shortcode);
                                        @endphp
                                    </select>
                                </div>

                                <div class="wrap-group">
                                    <select name="course-price" id="course-price">
                                        <option value="">{{ _e('Cost', 'duhoc') }}</option>

                                        <option value="end">
                                            < {{ format_price(get_option('price_approx_end')) }} đ
                                        </option>
                                        <option value="mot">
                                            {{ format_price(get_option('price_approx_1_from')) }} đ - {{ format_price(get_option('price_approx_1_to')) }} đ
                                        </option>
                                        <option value="hai">
                                            {{ format_price(get_option('price_approx_2_from')) }} đ - {{ format_price(get_option('price_approx_2_to')) }} đ
                                        </option>
                                        <option value="star">
                                            >  {{ format_price(get_option('price_approx_star')) }} đ
                                        </option>

                                    </select>
                                </div>

                                <div class="wrap-group">
                                    <input type="submit" value="{{ _e('Search', 'duhoc') }}" name="search-course-submit">
                                </div>

                            </div>

                        </form>
                    </div>
                    
                    <section class="page-list-course">
                        <div class="page-list-course-content">

                            @php
                                $shortcode = "[listing post_type='courses' taxonomy='courses-category(".$id_page.")' layout='partials.sections.content-taxonomy-courses' paged='yes' per_page='4' ]";
                                echo do_shortcode($shortcode);
                            @endphp

                        </div>
                    </section>

                    @php
                        view('partials/page-next-steps');
                    @endphp
                    
                    @empty (!$page_post)
                    @php
                        $shortcode = "[page_post id=$page_post title='$page_post_title' numberpost url='partials.sections.content-page-post']";
                        echo do_shortcode($shortcode);
                    @endphp
                    @endempty

                </div>

                @php
                    view('sidebar-course');
                @endphp

            </div>

        </div>
    </section>
@endsection