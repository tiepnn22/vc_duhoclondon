@extends('layouts.full-width')

@section('banner')

    @if (!empty($banner_img_check))
        <img src="{{ $banner_img_check }}">
    @endif

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-contactus">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-contactus-content">

                    <section class="content-page">
                        <div class="page-title">
                            @empty (!$name_page)
                                <h1>{{ $name_page }}</h1>
                            @endempty
                        </div>

                        @empty (!get_the_content())
                        <div class="page-desc">
                            {!! wpautop(the_content()) !!}
                        </div>
                        @endempty
                    </section>

                </div>

				<?php get_sidebar();?>

            </div>
        </div>
    </section>

    @endwhile
@endsection