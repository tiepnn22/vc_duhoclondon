@extends('layouts.full-width')

@section('banner')

    @if (!empty($banner_img_check))
        <img src="{{ $banner_img_check }}">
    @endif

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

    <section class="page-school">
        <div class="container">
            <div class="page-school-content">

            	<section class="content-page">
					<div class="page-title">
						<h1>{{ $name_page }}</h1>
					</div>

					<div class="title-article">
						<h3>{{ _e('See learning key in field', 'duhoc') }}</h3>
					</div>
					<form action="" method="get">
						<span>{{ _e('Enter college or university name in the box below', 'duhoc') }}</span>
						<input type="text" id="find-school" name="find-school" placeholder="{{ _e('Find a school', 'duhoc') }}">
					</form>

					<div class="title-article">
						<h3>{{ _e('Download information for schools around the world', 'duhoc') }}</h3>
					</div>
					<span>{{ _e('Click on a country to download the universities or colleges in that country.', 'duhoc') }}</span>
				</section>

				<section class="list-school">
				    <ul class="nav nav-tabs" role="tablist">
				        <li class="nav-item">
				            <a class="nav-link active" href="#school0" role="tab" data-toggle="tab">
				                {{ _e('All Countries', 'duhoc') }}
				            </a>
				        </li>

						@php
							$terms_school = get_terms('university-category', array(
							    'parent'=> 0,
							    'hide_empty' => false
							) );

							$i = 1;
							foreach( $terms_school as $terms_school_kq ) {
						@endphp
							    <li class="nav-item">
							        <a class="nav-link" href="#school{{$i}}" role="tab" data-toggle="tab">
							            @php echo $terms_school_kq->name; @endphp
							        </a>
							    </li>
					    @php
							    $i++;
							}
						@endphp
				    </ul>



				    <div class="tab-content">
				        <div role="tabpanel" class="tab-pane active" id="school0">
				        	<ul>
								@php
									$shortcode = "[listing post_type='university' layout='partials.sections.content-list-school' per_page=-1]";
									echo do_shortcode($shortcode);
								@endphp
							</ul>
				        </div>

						@php
							$terms_school = get_terms('university-category', array(
							    'parent'=> 0,
							    'hide_empty' => false
							) );

							$i = 1;
							foreach( $terms_school as $terms_school_kq ) {
						@endphp
						<div role="tabpanel" class="tab-pane" id="school{{$i}}">
							<ul>
							    
				            @php
				            	$id_term_school = $terms_school_kq->term_id;

								$shortcode = "[listing post_type='university' taxonomy='university-category(".$id_term_school.")' layout='partials.sections.content-list-school' per_page=-1]";
								echo do_shortcode($shortcode);
			            	@endphp
							    
							</ul>
						</div>
						@php
							    $i++;
							}
						@endphp

				    </div>
				</section>

            </div>
        </div>
    </section>

    @endwhile
@endsection