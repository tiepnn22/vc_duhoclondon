@extends('layouts.full-width')
@section('content')
    <section class="page-course">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-course-content">
                    <section class="content-page">
                        <div class="page-title">
                            @empty (!$name_page)
                                <h1>{{ $name_page }}</h1>
                            @endempty
                        </div>
                    </section>
                    <div class="search-course">
                        <form action="{{ get_data_language( '/search-khoa-hoc/', '/en/search-course/' ) }}" method="post" class="form-search-course">
                            <div class="order-course">
                                {{ _e('Sorted by:', 'duhoc') }}
                                <select name="course-order" id="course-order">
                                    <option
                                        @empty (!$sap_xep)
                                            @php
                                                if($sap_xep == 'DESC') { echo 'selected="selected"'; }
                                            @endphp
                                        @endempty
                                         value="DESC">{{ _e('Latest:', 'duhoc') }}</option>
                                    <option
                                        @empty (!$sap_xep)
                                            @php
                                                if($sap_xep == 'ASC') { echo 'selected="selected"'; }
                                            @endphp
                                        @endempty
                                        value="ASC">{{ _e('Oldest:', 'duhoc') }}</option>
                                </select>
                            </div>
                            <div class="wrap-group-course">
                                <div class="wrap-group">
                                    @php
                                        $data = [
                                            'term_slug' => 'mon-hoc',
                                            'term_slug_en' => 'subject',
                                            'select_name' => 'course-subjects',
                                            'mon_hoc' => $mon_hoc,
                                            'bac_hoc' => $bac_hoc,
                                        ];
                                    @endphp
                                    {!!  view('partials.sections.content-select-search-course', $data)  !!}
                                </div>
                                <div class="wrap-group">
                                    @php
                                        $data = [
                                            'term_slug' => 'bac-hoc',
                                            'term_slug_en' => 'level-learning',
                                            'select_name' => 'course-level',
                                            'mon_hoc' => $mon_hoc,
                                            'bac_hoc' => $bac_hoc,
                                        ];
                                    @endphp
                                    {!!  view('partials.sections.content-select-search-course', $data)  !!}
                                </div>
                                <div class="wrap-group">
                                    <select name="course-country" id="course-country">
                                        <option value="">{{ _e('Country', 'duhoc') }}</option>
                                        @foreach( $terms_school as $terms_school_kq )
                                            <option
                                                @empty (!$quoc_gia)
    				                                @php
                                                        if ($quoc_gia == $terms_school_kq->term_id) {echo 'selected="selected"';}
                                                    @endphp
    				                            @endempty
                                             value="{{ $terms_school_kq->term_id }}">{{ $terms_school_kq->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="wrap-group">
                                    <select name="course-university" id="course-university" requiredmsg="{{ get_data_language( 'Vui lòng chọn trường', 'Please select a school' ) }}">
                                        <option value="">{{ _e('University', 'duhoc') }}</option>
                                        @php
                                            $shortcode = "[listing post_type='university' layout='partials.sections.content-select-search-course-school' per_page=-1]";
                                            echo do_shortcode($shortcode);
                                        @endphp
                                    </select>
                                </div>
                                <div class="wrap-group">
                                    <select name="course-price" id="course-price">
                                        <option value="">{{ _e('Cost', 'duhoc') }}</option>

                                        <option value="end" @php if($chi_phi == "end") { echo 'selected="selected"'; } @endphp>
                                            < {{ format_price(get_option('price_approx_end')) }} đ
                                        </option>
                                        <option value="mot" @php if($chi_phi == "mot") { echo 'selected="selected"'; } @endphp>
                                            {{ format_price(get_option('price_approx_1_from')) }} đ - {{ format_price(get_option('price_approx_1_to')) }} đ
                                        </option>
                                        <option value="hai" @php if($chi_phi == "hai") { echo 'selected="selected"'; } @endphp>
                                            {{ format_price(get_option('price_approx_2_from')) }} đ - {{ format_price(get_option('price_approx_2_to')) }} đ
                                        </option>
                                        <option value="star" @php if($chi_phi == "star") { echo 'selected="selected"'; } @endphp>
                                            >  {{ format_price(get_option('price_approx_star')) }} đ
                                        </option>

                                    </select>
                                </div>
                                <div class="wrap-group">
                                    <input type="submit" value="{{ _e('Search', 'duhoc') }}" name="search-course-submit">
                                </div>
                            </div>
                        </form>
                    </div>
                    <section class="page-list-course">
                        <div class="page-list-course-content">
						@php
                            if ( $search_result->have_posts() ) :
    							while ($search_result->have_posts()): $search_result->the_post();
                                    $data = [
                                    	'id' => get_the_ID(),
                                        'title' => get_the_title(),
                                        'url' => get_the_permalink(),
                                    ];
                                    @endphp
                                    {!!  view('partials.sections.content-taxonomy-courses', $data)  !!}
                                    @php
    							endwhile; wp_reset_postdata();
                            else:
                                echo '<span><strong>' . (_e('Result is empty', 'duhoc')) . '</strong></span>';
                            endif;
						@endphp
                        </div>
                    </section>
                    @php
                        view('partials/page-next-steps');
                    @endphp
                </div>
                @php
                    view('sidebar-course');
                @endphp
            </div>
        </div>
    </section>
@endsection










