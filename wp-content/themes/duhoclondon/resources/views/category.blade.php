@extends('layouts.full-width')

@section('banner')

    @if (!empty($banner_img_check))
        <img src="{{ $banner_img_check }}">
    @endif

@endsection

@section('content')
    @while(have_posts())
		{!! the_post() !!}

	<section class="page-category">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content">
	                
	                <section class="content-page">
	                    <div class="page-title">
	                        <h1>{{ $name_page }}</h1>
	                    </div>
	                </section>

	                @empty (!$id_page)
                    @php
                        $shortcode = "[page_post id=$id_page title='$page_post_title' numberpost url='partials.sections.content-page-post']";
                        echo do_shortcode($shortcode);
                    @endphp                
	                @endempty

                    @php
                        view('partials/page-next-steps');
                    @endphp

	            </div>

	            <?php get_sidebar();?>

	        </div>
	    </div>
	</section>

    @endwhile
@endsection
