@extends('layouts.full-width')

@section('banner')

    @php
    	$banner_img_check = "a";
    @endphp
    <img src="{{ asset_image('banner-travel.jpg') }}">

@endsection

@section('content')

    @while(have_posts())
	
		{!! the_post() !!}

        @include('partials.content-single-' . get_post_type())

    @endwhile

@endsection
