@extends('layouts.full-width')


@section('banner')

    @empty (!$banner_img_check)
        <img src="{{ $banner_img_check }}">
    @endempty

@endsection

@section('content')
    @while(have_posts())
        {!! the_post() !!}

	    <section class="page-ielts">
	        <div class="container">
	            <div class="row">
	                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-ielts-content">
                        
                        <section class="content-page">
                            <div class="page-title">
                                @empty (!$name_page)
                                    <h1>{{ $name_page }}</h1>
                                @endempty
                            </div>

                            @empty (!get_the_content())
                            <div class="page-desc">
                                {!! wpautop(the_content()) !!}
                            </div>
                            @endempty
                        </section>

                        @empty (!$page_post)
                        @php
                            $shortcode = "[page_post id=$page_post title='$page_post_title' numberpost url='partials.sections.content-page-post']";
                            echo do_shortcode($shortcode);
                        @endphp
                        @endempty

                        @empty (!$page_post_recent)
                        @php
                            $shortcode = "[page_post_recent id=$page_post_recent numberpost url='partials.sections.content-page-post-recent']";
                            echo do_shortcode($shortcode);
                        @endphp
                        @endempty

                        @empty (!$page_post_recent_two)
                        @php
                            $shortcode = "[page_post_recent id=$page_post_recent_two numberpost url='partials.sections.content-page-post-recent']";
                            echo do_shortcode($shortcode);
                        @endphp
                        @endempty

                        @php
                            view('partials/page-next-steps');
                        @endphp
                        
                    </div>

                    <?php get_sidebar();?>

                </div>
            </div>
        </section>

    @endwhile
@endsection