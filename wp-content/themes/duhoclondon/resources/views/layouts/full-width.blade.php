<!DOCTYPE html>

<html {!! language_attributes() !!}>

  @include('partials.head')

    <body {!! body_class() !!}>

    {!! do_action('get_header') !!}

    @include('partials.header')

    <div class="wrap fluid-container" role="document">

        <section class="banner-page">
            @yield('banner')
        
            @if( is_home() || is_front_page() )

            @else
                <div class="<?php if( !empty($banner_img_check) ) { echo "breadcrumbs-position"; } ?>">
                    <div class="breadcrumbs">
                        <div class="container">
                            @php
                                if(function_exists('bcn_display')) { 
                                    echo '<a href="' . site_url() . '"><i class="fa fa-home"></i> </a> > ';
                                    bcn_display(); 
                                }
                            @endphp
                        </div>
                    </div>
                </div>
            @endif
        </section>

        <div class="content">
            <main class="main">
                @yield('content')
            </main>
        </div>
        
    </div>

    {!! do_action('get_footer') !!}

    @include('partials.footer')

    {!! wp_footer() !!}

    </body>
    
</html>
