<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class NextStep extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('next_step', 'duhoc'),
            'label'       => __('Next steps', 'duhoc'),
            'description' => __('Next steps', 'duhoc'),
        ];

        $fields = [
            [
                'label' => __('Title', 'duhoc'),
                'name'  => 'title',
                'type'  => 'text',
            ],
            [
                'label' => __('Image', 'duhoc'),
                'name'  => 'image',
                'type'  => 'text',
            ],
            // [
            //     'label' => __('Title box', 'duhoc'),
            //     'name'  => 'title-box',
            //     'type'  => 'text',
            // ],
            [
                'label' => __('Text Url', 'duhoc'),
                'name'  => 'readmore',
                'type'  => 'text',
            ],
            [
                'label' => __('Url', 'duhoc'),
                'name'  => 'url',
                'type'  => 'text',
            ]
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <div class="item">
                <figure>
                    <a href="<?php echo $instance['url']; ?>">
                        <img src="<?php echo $instance['image']; ?>" >
                    </a>
                </figure>
                <div class="info">
                    <div class="info-content">
                        <div class="title">
                            <a href="<?php echo $instance['url']; ?>">
                                <?php echo $instance['title']; ?>
                            </a>
                        </div>
                        <a class="read-more" href="<?php echo $instance['url']; ?>">
                            <?php echo $instance['readmore']; ?>
                        </a>
                    </div>
                </div>
            </div>
        <?php
	}
}
