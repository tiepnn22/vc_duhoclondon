<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class SupportOnline extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('support_online', 'duhoc'),
            'label'       => __('Free Consultation', 'duhoc'),
            'description' => __('Free Consultation', 'duhoc'),
        ];

        $fields = [
            [
                'label' => __('Title', 'duhoc'),
                'name'  => 'title',
                'type'  => 'text',
            ],
            [
                'label' => __('Image', 'duhoc'),
                'name'  => 'image',
                'type'  => 'text',
            ],
            [
                'label' => __('Text Url', 'duhoc'),
                'name'  => 'readmore',
                'type'  => 'text',
            ],
            [
                'label' => __('Url', 'duhoc'),
                'name'  => 'url',
                'type'  => 'text',
            ]
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
            <section class="widget_media_image">
                <div class="title-widget">
                    <?php echo $instance['title']; ?>                    
                </div>
                <div class="next-steps">

                    <div class="item">
                        <figure>
                            <a href="<?php echo $instance['url']; ?>">
                                <img src="<?php echo $instance['image']; ?>" >
                            </a>
                        </figure>
                        <div class="info">
                            <div class="info-content">
                                <a class="read-more" href="<?php echo $instance['url']; ?>">
                                    <?php echo $instance['readmore']; ?>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        <?php
	}
}
