<?php

namespace App\Models;

use NF\Models\Model;

/**
 *
 */
class Term_taxonomy extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'term_taxonomy';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    protected $fillable = ['term_taxonomy_id', 'parent', 'count'];

}
