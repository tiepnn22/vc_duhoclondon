<?php

namespace App\Models;

use NF\Models\Model;

/**
 *
 */
class Terms extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'terms';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    protected $fillable = ['term_id', 'name', 'slug'];

}
