<?php

namespace App\Providers;

use App\Widgets\SampleWidget;
use App\Widgets\SupportOnline;
use App\Widgets\NextStep;
use Illuminate\Support\ServiceProvider;

class WidgetServiceProvider extends ServiceProvider
{
    public $listen = [
        SampleWidget::class,
        SupportOnline::class,
        NextStep::class,
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveWidget($class);
        }
    }

    /**
     * Resolve a widget instance from the class name.
     *
     * @param  string  $widget
     * @return widget instance
     */
    public function resolveWidget($widget)
    {
        return new $widget();
    }
}
