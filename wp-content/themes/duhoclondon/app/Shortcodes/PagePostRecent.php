<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class PagePostRecent extends ShortCode
{
    public $name = 'page_post_recent';

    public function render($attr)
    {
    	ob_start();

    	$per_page = $attr['numberpost'];
    	$layout = $attr['url'];
    	$cat = $attr['id'];

		$per_page = ( empty($per_page) ) ? 6 : $per_page;

		echo '
			<section class="page-post-recent">
		        <div class="title-section">
		            <h2>'.get_cat_name( $cat ).'</h2>
		        </div>

			    <div class="page-post-recent-content">
		    		<div class="row">';

			    	$shortcode = "[listing cat=$cat layout=$layout per_page=$per_page ]";
			        echo do_shortcode($shortcode);

		echo '
					</div>
		    	</div>
			</section>';

        $content = ob_get_contents();

        ob_end_clean();

		return $content;
		
    }
}
