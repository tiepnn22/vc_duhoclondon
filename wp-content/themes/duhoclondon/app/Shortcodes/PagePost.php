<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class PagePost extends ShortCode
{
    public $name = 'page_post';

    public function render($attr)
    {
    	ob_start();

    	$per_page = $attr['numberpost'];
    	$layout = $attr['url'];
    	$title = $attr['title'];
    	$cat = $attr['id'];

		$per_page = ( empty($per_page) ) ? 6 : $per_page;
		
		echo '
			<section class="page-post">';
		    	if( !empty($title) ) {
    	echo '	
			        <div class="title-section">
			            <h2>'.$title.'</h2>
			        </div>';
		    	}
	    echo '
		    	<div class="page-post-content">';

			    	$shortcode = "[listing cat=$cat layout=$layout per_page=$per_page ]";
			        echo do_shortcode($shortcode);

		echo '
		    	</div>
			</section>';

        $content = ob_get_contents();

        ob_end_clean();

		return $content;
		
    }
}
