<?php

namespace App\Taxonomies;

use MSC\Tax;

class CoursesTaxonomy extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'courses-category',
			'single' => 'Courses Category',
			'plural' => 'Courses Category'
		];

		$postType = 'courses';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}