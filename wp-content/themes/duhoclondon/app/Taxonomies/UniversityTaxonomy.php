<?php

namespace App\Taxonomies;

use MSC\Tax;

class UniversityTaxonomy extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'university-category',
			'single' => 'University Category',
			'plural' => 'University Category'
		];

		$postType = 'university';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}