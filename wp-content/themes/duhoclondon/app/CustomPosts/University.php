<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class University extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'university';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'University';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'University';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-welcome-learn-more'];

}
