<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Pupils extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'pupils';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Pupils';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Pupils';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-groups'];

}
