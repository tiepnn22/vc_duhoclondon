<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Courses extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'courses';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Courses';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Courses';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-welcome-learn-more'];

}
