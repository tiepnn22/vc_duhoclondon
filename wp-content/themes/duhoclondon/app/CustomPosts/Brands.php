<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Brands extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'brands';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Brands';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Brands';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-image-filter'];

}
