<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
     */

    'view_path'  => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views',
    'cache_path' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'cache',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
     */
    'timezone'   => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
     */

    'providers'  => [
        \NightFury\Option\ThemeOptionServiceProvider::class,
        \VC\Slider\SliderServiceProvider::class,
        \NightFury\Social\SocialServiceProvider::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
     */

    'aliases'    => [

    ],

    'services'   => [
        'facebook' => [
            // 'client_id'     => '184779232154524',
            // 'client_secret' => '05dbc2ac4210577f4dd43dd88d1a96bc',
            // 'redirect'      => 'http://duhoclondon.local/oauth?provider=facebook',
            // 'client_id'     => '164961960679179',
            // 'client_secret' => '910ad85a0c92f8c64c55ecce42455582',
            // 'redirect'      => site_url() . '/login_fb?provider=facebook',
            'client_id'     => '349811632194610',
            'client_secret' => 'ea4c0d757c946b94a5c7f8b5c91219db',
            'redirect'      => site_url() . '/login_fb?provider=facebook',
        ],
        'google'  => [
            'client_id'     => '214043543810-s9oq4r89dgn194llquck5jkmrrr3d3mf.apps.googleusercontent.com',
            'client_secret' => 'H9H_HEquLnl8R3ZjmLMFaiNm',
            'redirect'      => site_url() . '/login_fb?provider=google', // use callback url from step 3
        ],
        'twitter'   => [
            'client_id'     => '',
            'client_secret' => '',
            'redirect'      => '',
        ],
    ],

];
