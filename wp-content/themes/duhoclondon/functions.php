<?php
// define('PREFIX_TABLE', $table_prefix );

// use App\Models\Terms;
// use Nf\Facade\Request;

$app = require_once __DIR__ . '/bootstrap/app.php';

// function search_course(Request $request) {

// 	if($request->has('search-course-submit')){
// 		$search_course = $request->get('search-course-submit');
// 	}
// 	$term = Terms::where('mon-hoc', $mon_hoc);
// 	$term = $term->where('bac-hoc', $bac_hoc);
// 	if($request->get('quoc-gia')){
// 		$term = $term->where('quoc-gia', $quoc_gia);
// 	}
// 	if($request->get('truong')){
// 		$term = $term->where('truong', $truong);
// 	}
// 	$term = $term->paginate(10);
// 	$term->name;
// }


add_action('wp_ajax_List_university', 'List_university');
add_action('wp_ajax_nopriv_List_university', 'List_university');
function List_university() {

	$value_course_country = $_POST['value_course_country'];
	$data = array();
	$data['result'] = '';

	$data_result .= '<option value="">'.get_data_language( 'Trường đại học', 'University' ).'</option>';

	if( isset($value_course_country) && !empty($value_course_country) ) {
	    $query =  new WP_Query( array(
	        'post_type' => 'university',
	        'tax_query' => array(
	                            array(
	                                    'taxonomy' => 'university-category',
	                                    'field' => 'id',
	                                    'terms' => $value_course_country,
	                                    'operator'=> 'IN'
	                             )),
	        'showposts'=> -1,
	        'order' => 'DESC',
	        'orderby' => 'date'
	    ) );
	} else {
	    $query =  new WP_Query( array(
	        'post_type' => 'university',
	        'showposts'=> -1,
	        'order' => 'DESC',
	        'orderby' => 'date'
	    ) );
	}

    if($query->have_posts()) {
        while ($query->have_posts() ) { $query->the_post();
        	$data_result .= '<option value="'.get_the_ID().'">'.get_the_title().'</option>';
        }
    }

    $data['result'] = $data_result;

    echo json_encode($data);
    die();

}

