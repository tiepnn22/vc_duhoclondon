<?php

$home_news_feature = get_field('home_news_feature');

$home_service_support = get_field('home_service_support');
$home_service_support_readmore = get_field('home_service_support_readmore');

$home_pupil_share_title = get_field('home_pupil_share_title');
$home_pupil_share = get_field('home_pupil_share');
$home_pupil_share_url = get_field('home_pupil_share_url');
$home_pupil_share_readmore = get_field('home_pupil_share_readmore');

$home_pupil_testimonial_title = get_field('home_pupil_testimonial_title');
$home_pupil_testimonial_bg = get_field('home_pupil_testimonial_bg');

$home_my_house_title = get_field('home_my_house_title');

$home_news_new = get_field('home_news_new');
$home_even_highlight = get_field('home_even_highlight');

$home_course_popular_title = get_field('home_course_popular_title');
$home_course_popular = get_field('home_course_popular');
$home_course_popular_url = get_field('home_course_popular_url');
$home_course_popular_readmore = get_field('home_course_popular_readmore');

$home_best_university_title = get_field('home_best_university_title');
$home_best_university_url = get_field('home_best_university_url');
$home_best_university_readmore = get_field('home_best_university_readmore');

$home_our_member_title = get_field('home_our_member_title');
$home_our_member = get_field('home_our_member');

$data = [
    
    'home_news_feature' => $home_news_feature,

    'home_service_support' => $home_service_support,
    'home_service_support_readmore' => $home_service_support_readmore,

    'home_pupil_share_title' => $home_pupil_share_title,
    'home_pupil_share' => $home_pupil_share,
    'home_pupil_share_url' => $home_pupil_share_url,
    'home_pupil_share_readmore' => $home_pupil_share_readmore,

    'home_pupil_testimonial_title' => $home_pupil_testimonial_title,
    'home_pupil_testimonial_bg' => $home_pupil_testimonial_bg,

    'home_my_house_title' => $home_my_house_title,

    'home_news_new' => $home_news_new,
    'home_even_highlight' => $home_even_highlight,
    
    'home_course_popular_title' => $home_course_popular_title,
    'home_course_popular' => $home_course_popular,
    'home_course_popular_url' => $home_course_popular_url,
    'home_course_popular_readmore' => $home_course_popular_readmore,

    'home_best_university_title' => $home_best_university_title,
    'home_best_university_url' => $home_best_university_url,
    'home_best_university_readmore' => $home_best_university_readmore,

    'home_our_member_title' => $home_our_member_title,
    'home_our_member' => $home_our_member,
];

view('front-page', $data);