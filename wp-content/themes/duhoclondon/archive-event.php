<?php
/**
 * Template Name: Sự kiện Template
 * 
 */

	$name_page = get_data_language( 'Sự kiện du học sắp diễn ra', 'The upcoming study event' );


	$data = [
	    'name_page' => $name_page
	];


	view('template-event', $data);
