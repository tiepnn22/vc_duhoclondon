<?php
    $terms_courses = get_terms( 'courses-category', array(
        'parent'=> 0,
        'hide_empty' => false 
    ) );

    $data = [
        'terms_courses' => $terms_courses
    ];


    view('archive-courses', $data);