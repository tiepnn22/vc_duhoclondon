<?php
/**
 * Template Name: Liên hệ Template
 * 
 */

	$pageId = get_the_ID();
	$banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
	$banner_img_check = $banner_img[0];

	$name_page = get_field('page_title', $pageId);

	$contact_map = get_field('contact_map', $pageId);


	$data = [
		'id_page' => $pageId,
	    'name_page' => $name_page,
	    'banner_img_check' => $banner_img_check,

	    'contact_map' => $contact_map
	];


	view('template-contact', $data);