<?php

    $current_category = get_category_by_slug( get_query_var( 'category_name' ) );
    $id_category = $current_category->term_id;
    $name_category = get_cat_name($id_category);

    $banner_img_check = get_field('image_cat','category_'.$id_category.'');

    if(empty($banner_img_check)) { $banner_img_check = asset_image('banner-travel.jpg'); }

	$data = [
		'id_page' => $id_category,
	    'name_page' => $name_category,
	    'banner_img_check' => $banner_img_check
	];


	view('category', $data);