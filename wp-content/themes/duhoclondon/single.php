<?php


	$page_post = get_field('page_post', $pageId);
	$page_post_title = get_field('page_post_title', $pageId);


	$data = [
	    'page_post' => $page_post,
	    'page_post_title' => $page_post_title
	];


	view('single', $data);