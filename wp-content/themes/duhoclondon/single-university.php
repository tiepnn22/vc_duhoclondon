<?php

	$pageId = get_the_ID();

	$name_page = get_the_title();

	$cat_school_info_avatar = get_field('cat_school_info_avatar', $pageId);

	$cat_school_info_col_1 = get_field('cat_school_info_col_1', $pageId);
	$cat_school_info_col_2 = get_field('cat_school_info_col_2', $pageId);
	$cat_school_info_col_3 = get_field('cat_school_info_col_3', $pageId);

	$cat_school_introduction = get_field('cat_school_introduction', $pageId);

	$cat_school_certificate_title = get_field('cat_school_certificate_title', $pageId);
	$cat_school_certificate_col_1 = get_field('cat_school_certificate_col_1', $pageId);
	$cat_school_certificate_col_2 = get_field('cat_school_certificate_col_2', $pageId);
	$cat_school_certificate_col_3 = get_field('cat_school_certificate_col_3', $pageId);

	$page_post_single_course_title = get_field('page_post_single_course_title', $pageId);
	$page_post_single_course = get_field('page_post_single_course', $pageId);
	$page_post_single_course_desc = get_field('page_post_single_course_desc', $pageId);


	$data = [
		'id_page' => $pageId,
	    'name_page' => $name_page,
	    
	    'cat_school_info_avatar' => $cat_school_info_avatar,

	    'cat_school_info_col_1' => $cat_school_info_col_1,
	    'cat_school_info_col_2' => $cat_school_info_col_2,
	    'cat_school_info_col_3' => $cat_school_info_col_3,

	    'cat_school_introduction' => $cat_school_introduction,
	    
	    'cat_school_certificate_title' => $cat_school_certificate_title,
	    'cat_school_certificate_col_1' => $cat_school_certificate_col_1,
	    'cat_school_certificate_col_2' => $cat_school_certificate_col_2,
	    'cat_school_certificate_col_3' => $cat_school_certificate_col_3,

	    'page_post_single_course_title' => $page_post_single_course_title,
	    'page_post_single_course' => $page_post_single_course,
	    'page_post_single_course_desc' => $page_post_single_course_desc,
	];


	view('single', $data);

?>