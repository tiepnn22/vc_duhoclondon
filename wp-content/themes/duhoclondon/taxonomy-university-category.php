<?php

	$term_tax = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$pageId = $term_tax->term_id;
	$name_page = $term_tax->name;

	$data = [
		'id_page' => $pageId,
		'name_page' => $name_page
	];


    view('taxonomy-university', $data);