<?php

	$term_tax = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$pageId = $term_tax->term_id;
	$name_page = $term_tax->name;

	$page_post = get_field('page_post', 'courses-category_'.$pageId);
	$page_post_title = get_field('page_post_title', 'courses-category_'.$pageId);


	$data = [
		'id_page' => $pageId,
		'name_page' => $name_page,
	    'page_post' => $page_post,
	    'page_post_title' => $page_post_title
	];


	view('taxonomy-course', $data);