<?php

class LoginDuHoc {
	
	public function __construct() {
		//thay doi text label form
		add_filter('login_form_defaults', [$this, 'wpse60605_change_username_label']);
		//add link quen mat khau
		add_action('login_form_middle', [$this, 'add_lost_password_link']);
	}

	public function wpse60605_change_username_label($defaults) {
		$defaults['redirect'] = admin_url();
		$defaults['form_id'] = 'loginform-duhoc';
		$defaults['label_username'] = __('Your Email Address');
		$defaults['label_password'] = __('Your Password');
		$defaults['label_remember'] = __('Remember password');
		$defaults['label_log_in'] = __('Login');
		$defaults['remember'] = false;
		return $defaults;
	}
	public function add_lost_password_link() {
		$forgot = '<p><a href="/wp-login.php?action=lostpassword">'.__('Forgot password?').'</a></p>';
		// return '<p><a href="/wp-login.php?action=lostpassword">Forgot password?</a></p>';
		return $forgot;
	}

}

new LoginDuHoc();