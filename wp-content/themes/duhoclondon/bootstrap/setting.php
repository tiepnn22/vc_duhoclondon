<?php

use NightFury\Option\Abstracts\Input;
use NightFury\Option\Facades\ThemeOptionManager;
use VC\Slider\Abstracts\Size;
use VC\Slider\Abstracts\SliderType;
use VC\Slider\Facades\SliderManager;
/**
 * Setting for theme
 */
class Setting
{
    public $path_css_menu = '';
    public $path_js_menu  = '';
    public $path_css_header  = '';
    public $path_css_footer  = '';
    public function __construct()
    {
        $this->configThemeOption();
        add_action('wp_enqueue_scripts', [$this, 'registerEnqueueScript']);

        $this->configSlider();
    }

    public function registerEnqueueScript()
    {
        if ($this->path_css_menu != null) {
            wp_enqueue_style(
                'menu-style',
                wp_slash($this->path_css_menu),
                false
            );
        }

        if ($this->path_js_menu != null) {
            wp_enqueue_script(
                'menu-scripts',
                wp_slash($this->path_js_menu),
                'jquery',
                '1.0',
                true
            );
        }

        if ($this->path_css_header != null) {
            wp_enqueue_style(
                'header-style',
                wp_slash($this->path_css_header),
                false
            );
        }

        if ($this->path_css_footer != null) {
            wp_enqueue_style(
                'footer-style',
                wp_slash($this->path_css_footer),
                false
            );
        }
    }

    public function configThemeOption()
    {
        $args = [
            'name'   => 'Configure Theme',
            'fields' => [
                [
                    'label'       => 'Css for menu',
                    'name'        => 'css_option_menu',
                    'type'        => Input::FILE,
                    'description' => 'Choose your .css file for menu by clicking the button bellow',
                ],
                [
                    'label'       => 'Js for menu',
                    'name'        => 'js_option_menu',
                    'type'        => Input::FILE,
                    'description' => 'Choose your .js file for menu by clicking the button bellow',
                ],
                [
                    'label'       => 'Css for Header',
                    'name'        => 'css_option_header',
                    'type'        => Input::FILE,
                    'description' => 'Choose your .css file for header by clicking the button bellow',
                ],
                [
                    'label'       => 'Css for Footer',
                    'name'        => 'css_option_footer',
                    'type'        => Input::FILE,
                    'description' => 'Choose your .css file for footer by clicking the button bellow',
                ],
            ],
        ];
        ThemeOptionManager::add($args);
        $this->path_css_menu  = get_option($args['fields'][0]['name']);
        $this->path_js_menu   = get_option($args['fields'][1]['name']);
        $this->path_css_header = get_option($args['fields'][2]['name']);
        $this->path_css_footer = get_option($args['fields'][3]['name']);
    }

    public function configSlider() {
    	$args = [
		    'name'    => 'Slider Homepage',
		    'type'    => SliderType::SLICK,
		    'style'   => 'style-1',
		    'size'    => Size::SIZE_3X1,
		    'fields'  => [
		        [
		            'label'       => 'Slider',
		            'name'        => 'slider',
		            'type'        => Input::GALLERY,
		            'description' => 'Gallery with meta field, for now we support text and textarea on meta field.',
		        ],
		    ],
		    'options' => [
		        'slidesToShow'   => 1,
		        'slidesToScroll' => 1,
		        'autoplay'       => false,
		        'pauseOnHover'   => true,
		        'infinite'       => false,
		        'dots'           => false,
		    ],
		];
    	SliderManager::add($args);
    }
}

new Setting;
