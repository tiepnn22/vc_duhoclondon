<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('app.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('app.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
        'ajax_url' => admin_url('admin-ajax.php', $protocol),
    );
    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus(array(
            'main-menu' => __('Main Menu', 'duhoc'),
        ));

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        // add_image_size('home-feature', 75, 75, true);
        add_image_size('home-service', 270, 180, true);
        add_image_size('home-find-course', 370, 270, true);
        add_image_size('home-pupil-read', 370, 300, true);
        add_image_size('home-news', 230, 220, true);
        add_image_size('home-member', 270, 150, true);
        add_image_size('page-post', 288, 210, true);
        add_image_size('page-post-recent', 250, 170, true);
        add_image_size('next-step', 120, 100, true);
        add_image_size('course', 480, 260, true);
        add_image_size('even', 370, 250, true);
        add_image_size('single-course', 386, 210, true);

    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Footer 1', 'duhoc'),
                'id'            => 'footer-1',
                'description'   => __('Footer 1', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Footer 2', 'duhoc'),
                'id'            => 'footer-2',
                'description'   => __('Footer 2', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Footer 3', 'duhoc'),
                'id'            => 'footer-3',
                'description'   => __('Footer 3', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Footer 4', 'duhoc'),
                'id'            => 'footer-4',
                'description'   => __('Footer 4', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Footer 5', 'duhoc'),
                'id'            => 'footer-5',
                'description'   => __('Footer 5', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Footer 6', 'duhoc'),
                'id'            => 'footer-6',
                'description'   => __('Footer 6', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],

            [
                'name'          => __('Sidebar Common', 'duhoc'),
                'id'            => 'sidebar-common',
                'description'   => __('Sidebar Common', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Sidebar Common En', 'duhoc'),
                'id'            => 'sidebar-common-en',
                'description'   => __('Sidebar Common en', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title">',
                'after_title'   => '</div>',
            ],

            [
                'name'          => __('Sidebar Course', 'duhoc'),
                'id'            => 'sidebar-course',
                'description'   => __('Sidebar Course', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="course-form-title">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Sidebar Single Even', 'duhoc'),
                'id'            => 'sidebar-single-even',
                'description'   => __('Sidebar Single Even', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title-widget">',
                'after_title'   => '</div>',
            ],

            [
                'name'          => __('Sidebar Next Step', 'duhoc'),
                'id'            => 'sidebar-next-step',
                'description'   => __('Sidebar Next Step', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title-widget">',
                'after_title'   => '</div>',
            ],
            [
                'name'          => __('Sidebar Next Step En', 'duhoc'),
                'id'            => 'sidebar-next-step-en',
                'description'   => __('Sidebar Next Step en', 'duhoc'),
                'before_widget' => '<div id="%1$s" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="title-widget">',
                'after_title'   => '</div>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
    function registerCustomizeFields() {
        $data = [
            [
                'info' => [
                    'name' => 'header',
                    'label' => 'Header',
                    'description' => '',
                    'priority' => 1,
                ],
                'fields' => [
                    [
                        'name' => 'logo',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Logo',
                    ],
                    [
                        'name' => 'slogan',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan',
                    ],
                    [
                        'name' => 'slogan_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan en',
                    ],
                    [
                        'name' => 'url_address',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link Address',
                    ],
                    [
                        'name' => 'url_address_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link address en',
                    ],
                    [
                        'name' => 'url_calendar',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link calendar',
                    ],
                    [
                        'name' => 'url_calendar_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link calendar en',
                    ],
                    [
                        'name' => 'url_register',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link register',
                    ],
                    [
                        'name' => 'url_register_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link register en',
                    ],
                    [
                        'name' => 'tell',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Tell',
                    ],
                ],
            ],
            [
                'info' => [
                    'name' => 'price',
                    'label' => 'Price approx',
                    'description' => '',
                    'priority' => 2,
                ],
                'fields' => [
                    [
                        'name' => 'approx_end',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Từ 0 đến',
                    ],
                    [
                        'name' => 'approx_1_from',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Khoảng 1 từ',
                    ],
                    [
                        'name' => 'approx_1_to',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Khoảng 1 đến',
                    ],
                    [
                        'name' => 'approx_2_from',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Khoảng 2 từ',
                    ],
                    [
                        'name' => 'approx_2_to',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Khoảng 2 đến',
                    ],
                    [
                        'name' => 'approx_star',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Từ bao nhiêu trở đi',
                    ],
                ],
            ],
        ];

        $customizer = new MSC\Customizer\Customizer($data);
        $customizer->create();
    }

    add_action('init', 'registerCustomizeFields');
}