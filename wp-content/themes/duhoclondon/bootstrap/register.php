<?php

class RegisterDuHoc {

	public function __construct() {
		//call ajax
		add_action('wp_ajax_check_user_email', [$this, 'check_user_email']);
		add_action('wp_ajax_nopriv_check_user_email', [$this, 'check_user_email']);

		add_action('wp_ajax_check_user_name', [$this, 'check_user_name']);
		add_action('wp_ajax_nopriv_check_user_name', [$this, 'check_user_name']);

		add_action('admin_post_sign_up', [$this, 'prefix_admin_sign_up']);
		add_action('admin_post_nopriv_sign_up', [$this, 'prefix_admin_sign_up']);
		//sign up nay ben form action = admin post, trong input hidden
	}

	public function check_user_name() {
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];

		$username = $firstname.$lastname;

		if (username_exists($username)) {
			$check_user_register = __('User is existed');
			echo json_encode($check_user_register);
		} else {
			echo json_encode('true');
		}
		die();
	}

	public function check_user_email() {
		if (email_exists($_POST['email'])) {
			$check_email_register = __('Email is existed');
			echo json_encode($check_email_register);
		} else {
			echo json_encode('true');
		}
		die();
	}

	public function prefix_admin_sign_up() {

		if (isset($_POST['registerform-duhoc'])) {
			$firstname = esc_sql(trim($_POST['firstname']));
			$lastname = esc_sql(trim($_POST['lastname']));

			$username = $firstname.$lastname;

			$tel = esc_sql(trim($_POST['tel']));
			$email = esc_sql(trim($_POST['email']));
			$pass = esc_sql(trim($_POST['pass']));

			$page_url = esc_sql(trim($_POST['page_url']));
			$error_success_register = esc_sql(trim($_POST['error_success_register']));

			$user_id = wp_insert_user(
				array(
					'user_login' => $username,
					'user_email' => $email,
					'user_pass' => $pass,
					'description' => $tel,
					'role' => 'contributor'
				)
			);
			do_action('user_register', $user_id);
			$success_register = '
				<script>
					alert("'.$error_success_register.'");
					window.location.href = "'.$page_url.'";
				</script>
			';
			echo $success_register;
		}
	}
}

new RegisterDuHoc();