<?php
global $wpdb;

define('NFWP_DB_TABLE_PREFIX', $wpdb->prefix);

if (!function_exists('view')) {
    /**
     * Use template engine quicker
     * @param string $path
     * @param array $data
     * @param  boolean $echo
     * @return mixed
     */
    function view($path, $data = [], $echo = true)
    {
        if ($echo) {
            echo NF\View\Facades\View::render($path, $data);
        } else {
            return NF\View\Facades\View::render($path, $data);
        }
    }
}

if (!function_exists('asset')) {
    /**
     * Get resource uri
     * @param string
     */
    function asset($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $assets);
    }
}

if (!function_exists('asset_image')) {
    function asset_image($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/resources/assets/images/' . $assets);
    }
}

if (!function_exists('title')) {
    /**
     * Generate page title
     *
     * @return string
     */
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name . ' - ' . get_bloginfo('name');
        }

        if (is_404()) {
            return '404 page not found - ' . get_bloginfo('name');
        }

        return get_the_title() . ' - ' . get_bloginfo('name');
    }
}

if (!function_exists('createExcerptFromContent')) {
    /**
     * this function will create an excerpt from post content
     *
     * @param  string $content
     * @param  int    $limit
     * @param  string $readmore
     * @since  1.0.0
     * @return string $excerpt
     */
    function createExcerptFromContent($content, $limit = 50, $readmore = '...')
    {
        if (!is_string($content)) {
            throw new Exception("first parameter must be a string.");
        }

        if ($content == '') {
            throw new Exception("first parameter is not empty.");
        }

        if (!is_int($limit)) {
            throw new Exception("second parameter must be the number.");
        }

        if ($limit <= 0) {
            throw new Exception("second parameter must greater than 0.");
        }

        $words = explode(' ', $content);

        if (count($words) <= $limit) {
            $excerpt = $words;
        } else {
            $excerpt = array_chunk($words, $limit)[0];
        }

        return strip_tags(implode(' ', $excerpt)) . $readmore;
    }
}

if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset_image('no-image.png') : $img[0];
    }
}

add_filter('widget_text', 'do_shortcode');

function custom_text_pagination_listing($pagi)
{
    $pagi = [
        'tag'       => 'div',
        'class'     => 'pagination',
        'id'        => '',
        'prev_text' => '<i class="fa fa-angle-left"></i>',
        'next_text' => '<i class="fa fa-angle-right"></i>',
    ];
    return $pagi;
}

add_filter('paged_wrap', 'custom_text_pagination_listing');

function getCategories($tax)
{
    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}terms as terms
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id
            WHERE term_tax.taxonomy = '{$tax}'";

    $results = $wpdb->get_results($sql);

    $cates = ['' => ''];

    foreach ($results as $term) {
        $cates[$term->term_id] = $term->name;
    }

    // var_dump($cates);exit;

    return $cates;
}

function getThumbnailVideo($url)
{
    $strpos_url = strpos($url, '?v=') + 3;

    $substr_url = substr( $url,  $strpos_url, 100);

    $image_video = 'https://img.youtube.com/vi/'.$substr_url.'/mqdefault.jpg';

    return $image_video;
}

if( !is_admin() ) {
    function filter_search($query) {
        if ($query->is_search) {
            $query->set('post_type', array('post', 'courses', 'evens'));
        };
        return $query;
    };
    add_filter('pre_get_posts', 'filter_search');
}

add_action('pre_get_posts', function ($query) {
    if ($query->is_search) {
        $query->set('posts_per_page', 6);
    }
});

function format_price($money)
{
    $str = "";
    if ($money != 0) {
        $num = (float) $money;
        $str = number_format($num, 0, '.', '.');
    }
    return $str;
}

if (!function_exists('get_data_language')) {
    function get_data_language($dataVN, $dataEN) {
        if(ICL_LANGUAGE_CODE == 'vi'){
            $get_data_language = $dataVN;
        } elseif (ICL_LANGUAGE_CODE == 'en') {
            $get_data_language = $dataEN;
        }
        return $get_data_language;
    }
}