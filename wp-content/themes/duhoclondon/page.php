<?php


	$pageId = get_the_ID();
	$banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
	$banner_img_check = $banner_img[0];

	if(empty($banner_img_check)) { $banner_img_check = asset_image('banner-travel.jpg'); }

	$name_page = get_field('page_title', $pageId);

	$page_post = get_field('page_post', $pageId);
	$page_post_title = get_field('page_post_title', $pageId);
	$page_post_recent = get_field('page_post_recent', $pageId);


	$data = [
	    'name_page' => $name_page,
	    'banner_img_check' => $banner_img_check,

	    'page_post' => $page_post,
	    'page_post_title' => $page_post_title,
	    'page_post_recent' => $page_post_recent
	];


	view('page', $data);
