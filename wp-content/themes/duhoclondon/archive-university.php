<?php
    $terms_school = get_terms( 'university-category', array(
        'parent'=> 0,
        'hide_empty' => false 
    ) );

    $data = [
        'terms_school' => $terms_school
    ];


    view('archive-university', $data);