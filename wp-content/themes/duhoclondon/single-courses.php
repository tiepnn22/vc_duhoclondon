<?php

	$pageId = get_the_ID();

	$name_page = get_the_title();

	$single_course_info_avatar = get_field('single_course_info_avatar', $pageId);

	$single_course_info_col_1 = get_field('single_course_info_col_1', $pageId);
	$single_course_info_col_2 = get_field('single_course_info_col_2', $pageId);
	$single_course_info_col_3 = get_field('single_course_info_col_3', $pageId);

	$single_course_introduction = get_field('single_course_introduction', $pageId);

	$single_course_certificate_title = get_field('single_course_certificate_title', $pageId);
	$single_course_certificate_col_1 = get_field('single_course_certificate_col_1', $pageId);
	$single_course_certificate_col_2 = get_field('single_course_certificate_col_2', $pageId);
	$single_course_certificate_col_3 = get_field('single_course_certificate_col_3', $pageId);
	$single_course_certificate_excerpt = get_field('single_course_certificate_excerpt', $pageId);

	$single_course_tuition_meta = get_field('single_course_tuition_meta', $pageId);
	$single_course_tuition_info = get_field('single_course_tuition_info', $pageId);

	$single_course_ratings_title = get_field('single_course_ratings_title', $pageId);
	$single_course_ratings_col_1 = get_field('single_course_ratings_col_1', $pageId);
	$single_course_ratings_col_2 = get_field('single_course_ratings_col_2', $pageId);

	$page_post_single_course_title = get_field('page_post_single_course_title', $pageId);
	$page_post_single_course = get_field('page_post_single_course', $pageId);
	$page_post_single_course_desc = get_field('page_post_single_course_desc', $pageId);


	$data = [
		'id_page' => $pageId,
	    'name_page' => $name_page,
	    
	    'single_course_info_avatar' => $single_course_info_avatar,

	    'single_course_info_col_1' => $single_course_info_col_1,
	    'single_course_info_col_2' => $single_course_info_col_2,
	    'single_course_info_col_3' => $single_course_info_col_3,

	    'single_course_introduction' => $single_course_introduction,

	    'single_course_certificate_title' => $single_course_certificate_title,
	    'single_course_certificate_col_1' => $single_course_certificate_col_1,
	    'single_course_certificate_col_2' => $single_course_certificate_col_2,
	    'single_course_certificate_col_3' => $single_course_certificate_col_3,
	    'single_course_certificate_excerpt' => $single_course_certificate_excerpt,

	    'single_course_tuition_meta' => $single_course_tuition_meta,
	    'single_course_tuition_info' => $single_course_tuition_info,

	    'single_course_ratings_title' => $single_course_ratings_title,
	    'single_course_ratings_col_1' => $single_course_ratings_col_1,
	    'single_course_ratings_col_2' => $single_course_ratings_col_2,

	    'page_post_single_course_title' => $page_post_single_course_title,
	    'page_post_single_course' => $page_post_single_course,
	    'page_post_single_course_desc' => $page_post_single_course_desc,
	];


	view('single', $data);

?>