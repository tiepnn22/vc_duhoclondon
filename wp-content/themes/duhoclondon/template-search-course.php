<?php
/**
 * Template Name: Search course Template
 *
 */

global $wpdb;
$course_ids             = [];
$ids_school_by_nation = [];

if (isset($_POST['search-course-submit']) && !empty($_POST['search-course-submit'])) {
    $sap_xep  = $_POST['course-order'];
    $mon_hoc  = $_POST['course-subjects'];
    $bac_hoc  = $_POST['course-level'];
    $quoc_gia = $_POST['course-country'];
    $truong   = $_POST['course-university'];
    $chi_phi  = $_POST['course-price'];
}

// if (isset($quoc_gia) && !empty($quoc_gia)) {
//     $args_national = [
//         'post_type'   => 'university',
//         'post_status' => ['publish'],
//         'tax_query'   => [
//             [
//                 'taxonomy' => 'university-category',
//                 'field'    => 'id',
//                 'terms'    => $quoc_gia,
//                 'compare' => '='
//             ],
//         ]
//     ];
//     $get_quocgia = get_posts($args_national);
//     if (!empty($get_quocgia)) {
//         foreach ($get_quocgia as $key => $item):
//             $ids_school_by_nation[] = $item->ID;
//         endforeach;
//     }
// }
// // echo "<pre>";
// // var_dump('quocgia: ', $quoc_gia);
// // var_dump('ids: ', $ids_school_by_nation);

// if (isset($truong) && !empty($truong)) {
//     $ids_school_by_nation[] = $truong;
// }

//lay ra bang single_course_on_school trong postmeta gom nhieu record
$sql_query_courses = "SELECT * from " . $wpdb->prefix . "postmeta as pm WHERE meta_key LIKE 'single_course_on_school%' AND meta_value != ''";
$exe_query_courses = $wpdb->get_results($sql_query_courses);
//lay ra cac record la cac bai post khoa hoc
if (!empty($exe_query_courses)) {
    foreach ($exe_query_courses as $key => $item) {
        //lay ra dc id truong trong field single_course_on_school
        $get_metavalue = maybe_unserialize($item->meta_value);
        $id_school_in_course     = $get_metavalue[0];
        //neu trung chon truong -> lay bai post khoa hoc
        if ($id_school_in_course === $truong) {
            $course_ids[] = $item->post_id;
        }
    }
}

// echo "<pre>";
// var_dump('exe: ', $exe_query_courses);
// var_dump('post_ids: ', $post_ids);
// die;

// =================

$terms_school = get_terms('university-category', [
    'parent'     => 0,
    'hide_empty' => false,
]);

$args = [
    'post_type'      => 'courses',
    'posts_per_page' => -1,
    'tax_query'      => [
        'relation' => 'AND',
    ],
    'meta_query'     => [
        'relation' => 'AND',
    ],
];
if (!empty($post_ids)) {
    $args['post__in'] = $course_ids;
}

if (isset($mon_hoc) && !empty($mon_hoc)) {
    $args['tax_query'][] = [
        'taxonomy' => 'courses-category',
        'field'    => 'id',
        'terms'    => $mon_hoc,
        'operator' => 'IN',
    ];
}

if (isset($bac_hoc) && !empty($bac_hoc)) {
    $args['tax_query'][] = [
        'taxonomy' => 'courses-category',
        'field'    => 'id',
        'terms'    => $bac_hoc,
        'operator' => 'IN',
    ];
}

if (isset($truong) && !empty($truong)) {
    $args['meta_query'][] = [
        'key'     => 'single_course_on_school',
        'value'   => '"' . $truong . '"',
        'compare' => 'LIKE',
    ];
}

if (isset($chi_phi) && !empty($chi_phi)) {
    if ($chi_phi == "end") {
        $args['meta_query'][] = [
            'key'     => 'single_course_info_col_3',
            'value'   => get_option('price_approx_end'),
            'compare' => '<=',
            'type'    => 'numeric',
        ]; 
    } elseif ( $chi_phi == "star" ) {
        $args['meta_query'][] = [
            'key'     => 'single_course_info_col_3',
            'value'   => get_option('price_approx_star'),
            'compare' => '>=',
            'type'    => 'numeric',
        ]; 
    } elseif ( $chi_phi == "mot" ) {
        $args['meta_query'][] = [
            'key'     => 'single_course_info_col_3',
            'value'   => array( get_option('price_approx_1_from'), get_option('price_approx_1_to') ),
            'compare' => 'BETWEEN',
            'type'    => 'numeric',
        ]; 
    } elseif ( $chi_phi == "hai" ) {
        $args['meta_query'][] = [
            'key'     => 'single_course_info_col_3',
            'value'   => array( get_option('price_approx_2_from'), get_option('price_approx_2_to') ),
            'compare' => 'BETWEEN',
            'type'    => 'numeric',
        ]; 
    }
}

if (isset($sap_xep) && !empty($sap_xep)) {
    $args['order']   = $sap_xep;
    $args['orderby'] = 'date';
}

$search_result = new WP_Query($args);

// echo "<pre>";
// var_dump($search_result);
// die;

$data = [
    'sap_xep'       => $sap_xep,
    'mon_hoc'       => $mon_hoc,
    'bac_hoc'       => $bac_hoc,
    'quoc_gia'      => $quoc_gia,
    'truong'        => $truong,
    'chi_phi'       => $chi_phi,
    'terms_school'  => $terms_school,
    'search_result' => $search_result,
];

view('template-search-course', $data);
